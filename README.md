# Degree materials : RSW

This repository is a work in progress. All the materials from Year 2 sem 1 (2-1) until Year 3 sem 2 (3-2) of RSW (Bachelor of Software Engineering) can be found here. This will contain notes, attempted tutorials, practicals and past years.

Note: Some portions of this repository will be redirected to another project/repository due to various issues.