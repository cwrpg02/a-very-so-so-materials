import ShapeService.*;

public class SingletonDrawShape {
    private Rectangular rect;
    private Circle cir;
    private Square sq;

    // singleton step 2: object as attribute
    // new FacadeDrawShape --> execute 1 time
    private static SingletonDrawShape sds = new SingletonDrawShape();

    // singleton step 1: private default constructor
    private SingletonDrawShape() {
        rect = new Rectangular();
        cir = new Circle();
        sq = new Square();
    }

    // singleton step 3: create entry point oaccess object
    public static SingletonDrawShape getInstance() {
        // return the single copy of the object to the client
        return sds;
    }

    public void drawCircle() {
        cir.draw();
    }
    public void drawRectangle() {
        rect.draw();
    }
    public void drawSquare() {
        sq.draw();
    }
}