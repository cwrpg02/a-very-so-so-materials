public class ShapeClient {
    public static void main(String[] args) {
        SingletonDrawShape sds = SingletonDrawShape.getInstance();
        
        sds.drawCircle();
        sds.drawRectangle();
        sds.drawSquare();
    }
}
