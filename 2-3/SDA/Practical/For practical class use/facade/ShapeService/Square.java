package facade.ShapeService;

public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("Draw in square");
    }
    
    @Override
    public void calculate() {
        System.out.println("Calculate in square");
    }
}
