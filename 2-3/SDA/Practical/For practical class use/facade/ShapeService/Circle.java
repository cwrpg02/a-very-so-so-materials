package facade.ShapeService;

public class Circle implements Shape {
    @Override
    public void draw() {
        System.out.println("Draw in circle");
    }
    
    @Override
    public void calculate() {
        System.out.println("Calculate in circle");
    }
}
