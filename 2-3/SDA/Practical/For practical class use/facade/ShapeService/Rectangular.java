package facade.ShapeService;

public class Rectangular implements Shape {
    @Override
    public void draw() {
        System.out.println("Draw in rectangular");
    }
    
    @Override
    public void calculate() {
        System.out.println("Calculate in rectangular");
    }
}
