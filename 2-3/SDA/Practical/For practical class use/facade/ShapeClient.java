package facade;

// importing component with service
//import facade.ShapeService.*;

public class ShapeClient {
    public static void main(String[] args) {
        //Circle objC = new Circle();
        //objC.draw();

        // Client communicate with facade
        FacadeShape objF = new FacadeShape();
        objF.drawCircle();
        objF.drawRectangle();
        objF.drawSquare();
    }
}
