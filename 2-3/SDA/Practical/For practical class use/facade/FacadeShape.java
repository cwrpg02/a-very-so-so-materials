package facade;
import facade.ShapeService.*;

// facade offer limited scope and specific function from the main components
// facade for draw only

public class FacadeShape {
    // attributes
    private Circle objC;
    private Rectangular objR;
    private Square objS;

    // constructors
    public FacadeShape() {
        objC = new Circle();
        objR = new Rectangular();
        objS = new Square();
    }

    public void drawCircle() {
        objC.draw();
    }
    public void drawRectangle() {
        objR.draw();
    }
    public void drawSquare() {
        objS.draw();
    }
}
// end of class