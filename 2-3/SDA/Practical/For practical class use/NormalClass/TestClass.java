package NormalClass;

public class TestClass {
    public static void main(String[] args) {
        ClassA objA = new ClassA();
        ClassB objB = new ClassB();

        objA.callSingle(); // initiate ClassSingler
        objB.callSingle(); // just call the same ClassSingler object use by ClassA
    }
}
