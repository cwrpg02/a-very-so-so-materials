package NormalClass;

public class ClassSingler {
    // object as attribute
    private static ClassSingler cs = new ClassSingler();
    private int a;

    // private default constructor
    private ClassSingler() {
        a = 10; // happen once
    }

    // method to get the object
    public static ClassSingler getInstance() {
        return cs;
    }

    public void display() {
        a += 10;
        System.out.println("Inside singler : " + a);
    }
}
