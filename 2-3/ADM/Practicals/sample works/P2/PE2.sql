--(a)
--inserting one recod into vehicle table
INSERT INTO Vehicle VALUES('WWW1111', 'Toyota', 'VOIS', 2016, 'Silver');

--list the variable details for vehicle table
desc vehicle

/*
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 VEHICLENO                                 NOT NULL VARCHAR2(7)
 VEHICLETYPE                                        VARCHAR2(20)
 MODEL                                              VARCHAR2(20)
 MAKEYEAR                                           NUMBER(4)
 COLOUR                                             VARCHAR2(20)
*/

--select the maximum job number from the job done table
SELECT MAX(JobNo) FROM JobDone;

/*
MAX(JOBNO)
----------
      1030
*/

--search the mechanic id where mechanic name assigned to akhbar khan from the mechanic table
SELECT MechanicID FROM Mechanic
WHERE MechanicName = 'Akhbar Khan';

/*
MECHANICID
----------
      2563
*/

--list the variable details for job done table
desc JobDone

/*
 Name                                      Null?    Type
 ----------------------------------------- -------- ----------------------------
 JOBNO                                     NOT NULL NUMBER(7)
 JDATE                                              DATE
 VEHICLENO                                          VARCHAR2(7)
 MECHANICID                                         NUMBER(4)
 WORKDONE                                           VARCHAR2(50)
 CHARGES                                            NUMBER(7,2)
*/

--inserting two records into job done table
INSERT INTO JobDone VALUES(1031, '2013-04-10', 'WWW1111', 2563, 'Change Engine 011', 168.00);
INSERT INTO JobDone VALUES(1032, '2013-04-10', 'WWW1111', 2563, 'Tyres Replaced', 920.00);






--(b)
SET linesize 120
SET pagesize 100
COLUMN JobNo HEADING 'Job No'
COLUMN JDate HEADING 'Date' FORMAT A11
COLUMN VehicleNo HEADING 'Place No' FORMAT A8
COLUMN MechanicID HEADING 'Mechanic ID'
COLUMN WorkDone HEAEDING 'Work Done' FORMAT A30
COLUMN Charges HEADING 'Charges (RM)' FORMAT $9999.99

SELECT J.*
FROM Mechanic M, JobDone J
WHERE M.MechanicID = J.MechanicID AND MechanicName = 'Michael Jack';

CLEAR COLUMNS;

/*
    Job No Date        Place No Mechanic ID WORKDONE                                           Charges (RM)
---------- ----------- -------- ----------- -------------------------------------------------- ------------
      1011 28-APR-12   KAK7852         5233 Tune-up                                                 $128.00
      1012 28-APR-12   JAC6598         5233 Rear Shock Absorber                                     $172.00
      1013 28-APR-12   ADT6528         5233 Wheel Chamber                                           $140.00
      1019 22-JUL-12   JDB8818         5233 Oil Change n Tune-up                                    $188.00
      1028 26-NOV-12   DAB8764         5233 Air-Cond Condenser                                      $550.00
*/






--(c)
CREATE TABLE Owner(
OwnerID		number(6) not null,
OwnerName	varchar(20) not null,
OwnerTel	varchar(14),
PRIMARY KEY (OwnerID)
);

ALTER TABLE Vehicle
ADD ownerID number(6);

ALTER TABLE Vehicle
ADD FOREIGN KEY (OwnerID) REFERENCES Owner(OwnerID);





--(d)
INSERT INTO Owner VALUES(100001, 'Roger Wong', '013-9876789');
INSERT INTO Owner VALUES(100002, 'John Tan', '012-1234567');
INSERT INTO Owner VALUES(100003, 'Eric Heng', '012-8888888');

UPDATE Vehicle
SET OwnerID = 100001
WHERE VehicleNo = 'MAA8523';

UPDATE Vehicle
SET OwnerID = 100002
WHERE VehicleNo = 'JDB8818';

UPDATE Vehicle
SET OwnerID = 100003
WHERE VehicleNo = 'WWW1111';