--data dictionary
--metadata: data types, NOT NULL, DEFAULT [value]

--Practical 7: CREATE & DROP INDEX
Select ProductCode, ProductName, MSRP, quantityInStock
from PRODUCTS
where ProductName LIKE '%FORD%';

/*
No rows selected
*/

Select ProductCode, ProductName, MSRP, quantityInStock
from PRODUCTS
where UPPER(ProductName) LIKE '%FORD%';

/*
PRODUCTCODE     PRODUCTNAME                                                                  MSRP QUANTITYINSTOCK
--------------- ---------------------------------------------------------------------- ---------- ---------------
S18_2248        1911 Ford Town Car                                                          60.54         -292
S18_2325        1932 Model A Ford J-Coupe                                                  127.13         8397
S18_2432        1926 Ford Fire Engine                                                       60.77         1020
S18_2949        1913 Ford Model T Speedster                                                101.31         3151
S18_2957        1934 Ford V8 Coupe                                                          62.46         4664
S18_3140        1903 Ford Model A                                                          136.59         3030
S18_3482        1976 Ford Gran Torino                                                      146.99         8212
S18_4600        1940s Ford truck                                                           121.08         2067
S32_4289        1928 Ford Phaeton Deluxe                                                    68.79         -836
S18_4933        1957 Ford Thunderbird                                                       71.27         2442
S24_3151        1912 Ford Model T Delivery Wagon                                            88.51         8182
S24_3816        1940 Ford Delivery Sedan                                                    83.86         5698
S12_1099        1968 Ford Mustang                                                          194.57         -865
S12_3891        1969 Ford Falcon                                                           173.02           84
S18_1097        1940 Ford Pickup Truck                                                     116.67         1614

15 rows selected.
*/

DROP INDEX prod_name_idx_func_idx;
CREATE INDEX prod_name_idx_func_idx ON PRODUCTS (UPPER(ProductName));

--Practical 7: SIMPLE & COMPLEX VIEW (Virtual Table)
--Main View
CREATE OR REPLACE VIEW Office1Staff AS
SELECT * 
FROM employees;

SELECT * FROM Office1Staff;

/*
EMPLOYEENUMBER LASTNAME                                           FIRSTNAME
-------------- -------------------------------------------------- --------------------------------------------------
EXTENSION  EMAIL
---------- ----------------------------------------------------------------------------------------------------
OFFICECODE  REPORTSTO JOBTITLE
---------- ---------- --------------------------------------------------
          1002 Murphy                                             Diane
x5800      dmurphy@classicmodelcars.com
1                     President

          1056 Patterson                                          Mary
x4611      mpatterso@classicmodelcars.com
1                1002 VP Sales

          1076 Firrelli                                           Jeff
x9273      jfirrelli@classicmodelcars.com
1                1002 VP Marketing

          1088 Patterson                                          William
x4871      wpatterson@classicmodelcars.com
6                1056 Sales Manager (APAC)

          1102 Bondur                                             Gerard
x5408      gbondur@classicmodelcars.com
4                1056 Sale Manager (EMEA)

          1143 Bow                                                Anthony
x5428      abow@classicmodelcars.com
1                1056 Sales Manager (NA)

          1165 Jennings                                           Leslie
x3291      ljennings@classicmodelcars.com
1                1143 Sales Rep

          1166 Thompson                                           Leslie
x4065      lthompson@classicmodelcars.com
1                1143 Sales Rep

          1188 Firrelli                                           Julie
x2173      jfirrelli@classicmodelcars.com
2                1143 Sales Rep

          1216 Patterson                                          Steve
x4334      spatterson@classicmodelcars.com
2                1143 Sales Rep

          1286 Tseng                                              Foon Yue
x2248      ftseng@classicmodelcars.com
3                1143 Sales Rep

          1323 Vanauf                                             George
x4102      gvanauf@classicmodelcars.com
3                1143 Sales Rep

          1337 Bondur                                             Loui
x6493      lbondur@classicmodelcars.com
4                1102 Sales Rep

          1370 Hernandez                                          Gerard
x2028      ghernande@classicmodelcars.com
4                1102 Sales Rep

          1401 Castillo                                           Pamela
x2759      pcastillo@classicmodelcars.com
4                1102 Sales Rep

          1501 Bott                                               Larry
x2311      lbott@classicmodelcars.com
7                1102 Sales Rep

          1504 Jones                                              Barry
x102       bjones@classicmodelcars.com
7                1102 Sales Rep

          1611 Fixter                                             Andy
x101       afixter@classicmodelcars.com
6                1088 Sales Rep

          1612 Marsh                                              Peter
x102       pmarsh@classicmodelcars.com
6                1088 Sales Rep

          1619 King                                               Tom
x103       tking@classicmodelcars.com
6                1088 Sales Rep

          1621 Nishi                                              Mami
x101       mnishi@classicmodelcars.com
5                1056 Sales Rep

          1625 Kato                                               Yoshimi
x102       ykato@classicmodelcars.com
5                1621 Sales Rep

          1702 Gerard                                             Martin
x2312      mgerard@classicmodelcars.com
4                1102 Sales Rep


23 rows selected.
*/

--Subview: Create Subview from Main View
CREATE OR REPLACE VIEW SimpleOffice1Staff AS
SELECT EmployeeNumber, LastName, FirstName, Email, officeCode
FROM Office1Staff;

SELECT * FROM SimpleOffice1Staff;

/*
EMPLOYEENUMBER LASTNAME                                           FIRSTNAME
-------------- -------------------------------------------------- --------------------------------------------------
EMAIL                                                                                                OFFICECODE
---------------------------------------------------------------------------------------------------- ----------
          1002 Murphy                                             Diane
dmurphy@classicmodelcars.com                                                                         1

          1056 Patterson                                          Mary
mpatterso@classicmodelcars.com                                                                       1

          1076 Firrelli                                           Jeff
jfirrelli@classicmodelcars.com                                                                       1

          1088 Patterson                                          William
wpatterson@classicmodelcars.com                                                                      6

          1102 Bondur                                             Gerard
gbondur@classicmodelcars.com                                                                         4

          1143 Bow                                                Anthony
abow@classicmodelcars.com                                                                            1

          1165 Jennings                                           Leslie
ljennings@classicmodelcars.com                                                                       1

          1166 Thompson                                           Leslie
lthompson@classicmodelcars.com                                                                       1

          1188 Firrelli                                           Julie
jfirrelli@classicmodelcars.com                                                                       2

          1216 Patterson                                          Steve
spatterson@classicmodelcars.com                                                                      2

          1286 Tseng                                              Foon Yue
ftseng@classicmodelcars.com                                                                          3

          1323 Vanauf                                             George
gvanauf@classicmodelcars.com                                                                         3

          1337 Bondur                                             Loui
lbondur@classicmodelcars.com                                                                         4

          1370 Hernandez                                          Gerard
ghernande@classicmodelcars.com                                                                       4

          1401 Castillo                                           Pamela
pcastillo@classicmodelcars.com                                                                       4

          1501 Bott                                               Larry
lbott@classicmodelcars.com                                                                           7

          1504 Jones                                              Barry
bjones@classicmodelcars.com                                                                          7

          1611 Fixter                                             Andy
afixter@classicmodelcars.com                                                                         6

          1612 Marsh                                              Peter
pmarsh@classicmodelcars.com                                                                          6

          1619 King                                               Tom
tking@classicmodelcars.com                                                                           6

          1621 Nishi                                              Mami
mnishi@classicmodelcars.com                                                                          5

          1625 Kato                                               Yoshimi
ykato@classicmodelcars.com                                                                           5

          1702 Gerard                                             Martin
mgerard@classicmodelcars.com                                                                         4


23 rows selected.
*/

--Option 1: Simple View that can modify without condition(s)
CREATE OR REPLACE VIEW SimpleOffice1Staff AS
SELECT EmployeeNumber, LastName, FirstName, Email, officeCode
FROM Office1Staff
WHERE OfficeCode = 1;

INSERT INTO SimpleOffice1Staff
VALUES(1800,'George','Gun','georgegun@classicmodelcars.com', 1);

SELECT * FROM employees WHERE employeeNumber = 1800;

/*
SQL> SELECT * FROM employees WHERE employeeNumber = 1800;

EMPLOYEENUMBER LASTNAME                                           FIRSTNAME
-------------- -------------------------------------------------- --------------------------------------------------
EXTENSION  EMAIL
---------- ----------------------------------------------------------------------------------------------------
OFFICECODE  REPORTSTO JOBTITLE
---------- ---------- --------------------------------------------------
          1800 George                                             Gun
           georgegun@classicmodelcars.com
1
*/

--Option 2: Simple View that can modify with condition(s)
CREATE OR REPLACE VIEW SimpleOffice1Staff AS
SELECT EmployeeNumber, LastName, FirstName, Email, officeCode
FROM Office1Staff
WHERE OfficeCode = 1
WITH CHECK OPTION CONSTRAINT limit_OfficeCode;

INSERT INTO SimpleOffice1Staff
VALUES(1801,'George','Stone','georgestone@classicmodelcars.com', 2);
---ORA-01402: view WITH CHECK OPTION where-clause violation
--code must match with WHERE statement

INSERT INTO SimpleOffice1Staff
VALUES(1801,'George','Stone','georgestone@classicmodelcars.com', 1);

--Option 3: Read Only View
CREATE OR REPLACE VIEW SimpleOffice1Staff AS
SELECT EmployeeNumber, LastName, FirstName, Email, officeCode
FROM Office1Staff
WHERE OfficeCode = 1
WITH READ ONLY CONSTRAINT simple_office1_readOnly;

INSERT INTO SimpleOffice1Staff
VALUES(1803,'George','Stone','georgestone@classicmodelcars.com',1);
---ORA-42399: cannot perform a DML operation on a read-only view

--Complex View
CREATE OR REPLACE VIEW high_order_view AS
SELECT customerNumber, O.orderNumber, orderDate, sum(priceEach * quantityOrdered) AS "Order Value", count(productCode) AS "No. of Products"
FROM orders O, orderDetails OD
WHERE  O.orderNumber = OD.orderNumber
GROUP BY customerNumber, O.orderNumber, orderDate
HAVING SUM( priceEach* quantityOrdered)>5000;

SELECT * FROM high_order_view;

/*
CUSTOMERNUMBER ORDERNUMBER ORDERDATE Order Value No. of Products
-------------- ----------- --------- ----------- ---------------
           489       10186 14-AUG-21    22275.73               9
           211       10200 01-SEP-21    17193.06               6
           202       10206 05-SEP-21    36527.61              11
           114       10223 20-NOV-21    44894.74              15
           381       10366 10-OCT-22     14379.9               3
           379       10369 20-OCT-22    28322.83               8
           398       10372 26-OCT-22    33967.73               9
           333       10374 02-NOV-22    21432.31               6
           382       10419 17-FEB-23    52420.07              14
           124       10421 28-FEB-23      7639.1               2
           128       10230 15-DEC-21    33820.62               8
           328       10233 29-DEC-21     7178.66               3
           181       10237 05-JAN-22    22602.36               9
           328       10251 18-FEB-22    31102.85               6
           201       10253 01-MAR-22    45443.54              14
           166       10259 15-MAR-22    44160.92              13
           357       10260 16-MAR-22    37769.38              10
           386       10266 06-APR-22    51619.02              15
           167       10289 03-JUN-22    12538.01               4
           189       10297 16-JUN-22    17359.53               7
           103       10298 27-JUN-22     6066.78               2
           286       10305 13-JUL-22    47411.33              14
           259       10310 16-JUL-22    61234.67              17
           141       10311 16-JUL-22    36140.38              11
           119       10315 29-JUL-22    19501.82               7
           462       10321 04-AUG-22    48355.87              15
           424       10337 21-AUG-22    25505.98               9
           381       10338 22-AUG-22    12081.52               3
           141       10358 10-SEP-22    44185.46              14
           141       10104 31-OCT-20     40206.2              13
           124       10135 02-APR-21    55601.84              17
           496       10138 07-APR-21    32077.44              13
           282       10139 16-APR-21    24013.52               8
           124       10142 08-MAY-21    56052.56              16
           276       10148 11-JUN-21    41554.73              14
           141       10153 28-JUN-21    44939.85              13
           424       10163 20-JUL-21    22042.37               6
           452       10164 21-JUL-21     27121.9               8
           324       10175 06-AUG-21    37455.77              12
           211       10187 15-AUG-21    28287.73              10
           471       10193 21-AUG-21    35505.63              16
           319       10195 25-AUG-21     36092.4              10
           141       10205 03-SEP-21    13059.16               5
           489       10213 22-OCT-21     7310.42               3
           256       10216 02-NOV-21     5759.42               1
           473       10218 09-NOV-21     7612.06               2
           314       10221 18-NOV-21    16901.38               5
           171       10224 21-NOV-21    18997.89               6
           161       10362 05-OCT-22    12692.19               4
           311       10373 31-OCT-22    46770.52              17
           119       10375 03-NOV-22    49523.67              15
           321       10381 17-NOV-22    32626.09               9
           321       10384 23-NOV-22    14155.57               4
           242       10397 28-DEC-22    12432.32               5
           145       10406 15-JAN-23    21638.62               3
           175       10413 05-FEB-23    28500.78               6
           412       10418 16-FEB-23    23627.44               9
           495       10243 26-JAN-22      6276.6               2
           141       10244 29-JAN-22    26155.91               9
           131       10248 07-FEB-22    41445.21              14
           173       10249 08-FEB-22    11843.45               5
           450       10250 11-FEB-22    42798.08              14
           471       10265 02-APR-22     9415.13               2
           204       10276 02-MAY-22    51152.86              14
           124       10282 20-MAY-22    47979.98              13
           415       10296 15-JUN-22    31310.09              14
           187       10306 14-JUL-22    52825.29              17
           339       10307 14-JUL-22    23333.06               9
           202       10313 22-JUL-22    33594.58              11
           363       10322 04-AUG-22    50799.69              14
           114       10342 24-AUG-22     40265.6              11
           278       10106 17-NOV-20    52151.81              18
           385       10108 03-DEC-20    51001.22              16
           114       10120 29-JAN-21    45864.03              15
           114       10125 21-FEB-21     7565.08               2
           198       10130 16-MAR-21     6036.96               2
           447       10131 16-MAR-21    17032.29               8
           353       10137 10-APR-21    13920.26               4
           161       10140 24-APR-21    38675.13              11
           320       10143 10-MAY-21    41016.75              16
           148       10150 19-JUN-21    38350.15              11
           321       10162 18-JUL-21    30876.44              10
           148       10165 22-JUL-21    67392.85              18
           496       10179 11-AUG-21     22963.6               9
           171       10180 11-AUG-21    42783.81              14
           146       10194 25-AUG-21     39712.1              11
           455       10196 26-AUG-21    38139.18               8
           216       10197 26-AUG-21    40473.86              14
           141       10203 02-SEP-21    40062.53              11
           406       10211 15-OCT-21    49165.16              15
           124       10229 11-DEC-21     43369.3              14
           334       10363 06-OCT-22    45785.34              15
           124       10371 23-OCT-22    35137.54              12
           124       10382 17-NOV-22    47765.59              13
           452       10392 10-DEC-22     8807.12               3
           124       10396 23-DEC-22    27695.54               8
           353       10398 30-DEC-22    46656.94              18

CUSTOMERNUMBER ORDERNUMBER ORDERDATE Order Value No. of Products
-------------- ----------- --------- ----------- ---------------
           496       10399 01-JAN-23    30253.75               8
           201       10403 08-JAN-23    37258.94               9
           450       10407 22-JAN-23    52229.55              12
           119       10425 28-FEB-23    41623.44              13
           260       10235 02-JAN-22    29284.42              12
           323       10254 03-MAR-22    37281.36              13
           398       10258 15-MAR-22    22037.91               6
           157       10281 19-MAY-22    39641.43              14
           448       10291 08-JUN-22     48809.9              14
           131       10292 08-JUN-22    35321.97              12
           249       10293 09-JUN-22    33924.24               9
           186       10299 30-JUN-22    34341.08              11
           201       10302 06-JUL-21    23908.24               6
           121       10309 15-JUL-22    17876.32               6
           456       10319 03-AUG-22    27550.51               9
           128       10323 05-AUG-22     7466.32               2
           181       10324 05-AUG-22     44400.5              14
           145       10327 10-AUG-22    20564.86               8
           216       10340 24-AUG-22    24945.21               8
           324       10351 03-SEP-22    13671.82               5
           250       10356 09-SEP-22    26311.63               9
           124       10357 10-SEP-22    40676.26              10
           496       10360 16-SEP-22       52166              18
           128       10101 09-OCT-20    10549.01               4
           131       10107 24-NOV-20    22292.62               8
           187       10110 18-DEC-20    48425.69              16
           353       10121 07-FEB-21    16700.47               5
           379       10147 05-JUN-21    32680.31              11
           161       10168 28-JUL-21    50743.65              18
           333       10174 06-AUG-21    23936.53               5
           363       10192 20-AUG-21    55425.77              16
           357       10202 02-SEP-21    20220.04               7
           151       10204 02-SEP-21    58793.53              17
           495       10207 09-SEP-21    59265.14              16
           141       10212 16-OCT-21    59830.55              16
           487       10219 10-NOV-21    12573.28               4
           239       10222 19-NOV-21    56822.65              18
           173       10228 10-DEC-21    20355.24               6
           282       10361 17-SEP-22    31835.36              14
           205       10367 12-OCT-22     39580.6              13
           141       10394 15-DEC-22    18102.74               7
           328       10401 03-JAN-23    43525.04              12
           177       10240 13-JAN-22    15183.63               3
           141       10246 05-FEB-22    35420.74              11
           406       10252 26-FEB-22    25080.96               9
           412       10268 12-APR-22    35034.57              11
           157       10272 20-APR-22     23715.7               6
           299       10284 21-MAY-22    32260.16              13
           128       10300 04-JUL-21    24101.81               8
           144       10320 03-AUG-22    16799.03               5
           187       10332 17-AUG-22    47159.11              18
           398       10339 23-AUG-22    48927.64              16
           114       10347 29-AUG-22    41995.62              12
           121       10103 29-OCT-20    50218.95              16
           486       10109 10-DEC-20    25833.14               6
           148       10117 16-JAN-21    44380.15              12
           103       10123 20-FEB-21    14571.44               4
           151       10127 03-MAR-21    58841.35              15
           141       10128 06-MAR-21    13884.99               4
           311       10151 21-JUN-21    32723.04              10
           473       10157 09-JUL-21    17746.26               6
           462       10166 21-JUL-21     9977.85               3
           175       10172 05-AUG-21    24879.08               8
           339       10183 13-AUG-21    34606.28              12
           167       10188 18-AUG-21    29954.91               8
           385       10198 27-AUG-21    20644.24               6
           475       10215 29-OCT-21    36070.47              10
           166       10217 04-NOV-21    22474.17               7
           189       10220 12-NOV-21    32538.74               9
           276       10370 20-OCT-22    27083.78               9
           186       10377 09-NOV-22     23602.9               6
           450       10400 01-JAN-23    31755.34               9
           314       10423 28-FEB-23     8597.73               5
           240       10232 20-DEC-21    24995.61               8
           145       10238 09-JAN-22     28211.7               8
           209       10241 13-JAN-22    36069.26              12
           334       10247 05-FEB-22    28394.54               6
           450       10257 14-MAR-22     16753.3               5
           175       10263 28-MAR-22    42044.77              11
           314       10273 21-APR-22    45352.47              15
           119       10275 23-APR-22    47924.19              18
           299       10301 05-JUL-21    36798.88              11
           256       10304 11-JUL-22    53116.99              17
           144       10334 19-AUG-22    23014.17               6
           124       10335 19-AUG-22     6466.44               3
           172       10336 20-AUG-22    51209.58              12
           353       10343 24-AUG-22    17104.91               6
           112       10346 29-AUG-22    14191.12               6
           458       10348 01-AUG-22    33145.56               8
           141       10355 07-SEP-22    25529.78              10
           363       10100 06-OCT-20    10223.83               4
           424       10115 04-JAN-21    21665.98               5
           350       10122 08-FEB-21    50824.66              17
           112       10124 21-FEB-21    32641.98              13
           458       10126 28-FEB-21    57131.92              17
           250       10134 01-APR-21    23419.47               7
           242       10136 04-APR-21     14232.7               3

CUSTOMERNUMBER ORDERNUMBER ORDERDATE Order Value No. of Products
-------------- ----------- --------- ----------- ---------------
           334       10141 01-MAY-21    29716.86               9
           447       10146 03-JUN-21     6631.36               2
           487       10149 12-JUN-21    29997.09              11
           321       10159 10-JUL-21    54682.68              18
           227       10161 17-JUL-21    36164.46              12
           278       10173 05-AUG-21    37723.79              16
           344       10177 07-AUG-21    31428.21              11
           475       10199 01-SEP-21     7678.25               3
           129       10201 01-SEP-21    23923.93               7
           347       10209 09-OCT-21    21053.69               8
           177       10210 12-OCT-21    47177.59              17
           298       10225 22-NOV-21    47375.92              14
           320       10365 07-OCT-22     8307.28               3
           124       10368 19-OCT-22    13874.75               5
           141       10378 10-NOV-22    32289.12              10
           141       10383 22-NOV-22    36851.98              13
           141       10386 01-DEC-22    46968.52              18
           448       10389 03-DEC-22    27966.54               8
           323       10393 11-DEC-22    33593.32              11
           323       10404 08-JAN-23    41426.81               8
           386       10416 10-FEB-23    35362.26              14
           412       10234 30-DEC-21    31670.37               9
           486       10236 03-JAN-22     5899.38               3
           141       10262 24-MAR-22    47065.36              16
           282       10270 19-APR-22    35806.73              11
           112       10278 06-MAY-22    33347.88              10
           260       10283 20-MAY-22    37527.58              14
           166       10288 01-JUN-22    38785.48              14
           319       10308 15-JUL-22    42339.76              16
           227       10314 22-JUL-22    53745.34              15
           278       10328 12-AUG-22    37654.09              14
           385       10330 16-AUG-22    15822.84               4
           129       10333 18-AUG-22    26248.78               8
           382       10341 24-AUG-22    42813.83              10
           350       10344 25-AUG-22    18888.31               7
           151       10349 01-SEP-22    39964.63              10
           447       10353 04-SEP-22    26304.13               9
           145       10105 11-NOV-20    53959.21              15
           129       10111 25-DEC-20    16537.85               6
           382       10119 28-JAN-21    35826.33              14
           333       10152 25-JUN-21     9821.32               4
           186       10155 06-JUL-21    37602.48              13
           233       10171 05-AUG-21    16909.84               4
           386       10176 06-AUG-21    38524.29              10
           124       10182 12-AUG-21    45084.38              17
           141       10190 19-AUG-21    10721.86               4
           259       10191 20-AUG-21    27988.47               9
           146       10227 02-DEC-21    40978.53              15
           141       10379 10-NOV-22    16621.27               5
           462       10388 03-DEC-22    30293.77               8
           124       10390 04-DEC-22     55902.5              16
           276       10391 09-DEC-22    29848.52              10
           406       10402 07-JAN-23    12190.85               3
           357       10410 29-JAN-23    36442.34               9
           141       10412 03-FEB-23    46895.48              11
           471       10415 09-FEB-23    10945.26               5
           141       10417 13-FEB-23     28574.9               6
           282       10420 28-FEB-23    42251.51              13
           311       10239 12-JAN-22    16212.59               5
           455       10245 04-FEB-22    32239.47               9
           233       10261 17-MAR-22    22997.45               9
           382       10269 16-APR-22     6419.84               2
           141       10279 09-MAY-22    20009.53               6
           286       10285 27-MAY-22    43134.04              13
           298       10287 30-MAY-22       61402              17
           362       10295 10-JUN-22    15059.76               5
           240       10316 01-AUG-22    46788.14              18
           131       10329 15-AUG-22    50025.35              15
           141       10350 02-SEP-22    46493.16              17
           124       10113 26-DEC-20     11044.3               4
           141       10133 27-MAR-21    22366.04               8
           448       10167 23-JUL-21    44167.09              16
           276       10169 04-AUG-21    38547.19              13
           242       10178 08-AUG-21    33818.34              12
           167       10181 12-AUG-21    55069.55              17
           484       10184 14-AUG-21    47513.19              13
           320       10185 14-AUG-21    52548.49              16
           146       10208 02-OCT-21    49614.72              15
           458       10214 26-OCT-21    22162.61               7
           239       10226 26-NOV-21    23552.59               7
           141       10380 16-NOV-22    34404.21              13
           250       10395 17-DEC-22    17928.09               4
           209       10405 14-JAN-23    35157.75               5
           233       10411 01-FEB-23    29070.38               9
           362       10414 06-FEB-23    50806.85              14
           157       10422 28-FEB-23     5849.44               2
           141       10424 28-FEB-23     29310.3               6
           344       10231 19-DEC-21    15322.93               2
           362       10264 31-MAR-22    18473.71               7
           151       10267 07-APR-22    20314.44               6
           124       10271 20-APR-22    37430.89              11
           379       10274 21-APR-22    12530.51               5
           249       10280 17-MAY-22    48298.99              17
           198       10290 07-JUN-22     5858.56               2
           124       10312 21-JUL-22    55639.66              17
           157       10318 02-AUG-22    35152.12               9
           121       10325 05-AUG-22    34638.14               9

CUSTOMERNUMBER ORDERNUMBER ORDERDATE Order Value No. of Products
-------------- ----------- --------- ----------- ---------------
           144       10326 09-AUG-22    19206.68               6
           486       10331 17-AUG-22    45994.07              14
           198       10352 03-SEP-22     9658.74               4
           323       10354 04-SEP-22    39440.59              13
           353       10359 15-SEP-22    32600.61               8
           181       10102 10-OCT-20     5494.78               2
           144       10112 24-DEC-20     7674.94               2
           172       10114 01-JAN-21    33383.14              10
           324       10129 12-MAR-21    29429.14               9
           205       10145 25-MAY-21    50342.74              16
           347       10160 11-JUL-21     20452.5               6
           452       10170 04-AUG-21    15130.97               4

303 rows selected.
*/

-Mix Actual Table and Virtual Table (View)
SELECT customerName, country, H.*
FROM customers C, high_order_view H
WHERE C.customerNumber = H.customerNumber;

/*
CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------
Double Decker Gift Stores, Ltd                     UK                                                    489
      10186 14-AUG-21    22275.73               9

King Kong Collectables, Co.                        Hong Kong                                             211
      10200 01-SEP-21    17193.06               6

Canadian Gift Exchange Network                     Canada                                                202
      10206 05-SEP-21    36527.61              11

Australian Collectors, Co.                         Australia                                             114
      10223 20-NOV-21    44894.74              15

Royale Belge                                       Belgium                                               381
      10366 10-OCT-22     14379.9               3

Collectables For Less Inc.                         USA                                                   379
      10369 20-OCT-22    28322.83               8

Tokyo Collectables, Ltd                            Japan                                                 398
      10372 26-OCT-22    33967.73               9

Australian Gift Network, Co                        Australia                                             333
      10374 02-NOV-22    21432.31               6

Salzburg Collectables                              Austria                                               382
      10419 17-FEB-23    52420.07              14

Mini Gifts Distributors Ltd.                       USA                                                   124
      10421 28-FEB-23      7639.1               2

Blauer See Auto, Co.                               Germany                                               128
      10230 15-DEC-21    33820.62               8

Tekni Collectables Inc.                            USA                                                   328
      10233 29-DEC-21     7178.66               3

Vitachrome Inc.                                    USA                                                   181
      10237 05-JAN-22    22602.36               9

Tekni Collectables Inc.                            USA                                                   328
      10251 18-FEB-22    31102.85               6

UK Collectables, Ltd.                              UK                                                    201
      10253 01-MAR-22    45443.54              14

Handji Gifts& Co                                   Singapore                                             166
      10259 15-MAR-22    44160.92              13

GiftsForHim.com                                    New Zealand                                           357
      10260 16-MAR-22    37769.38              10

L'ordine Souveniers                                Italy                                                 386
      10266 06-APR-22    51619.02              15

Herkku Gifts                                       Norway                                                167
      10289 03-JUN-22    12538.01               4

Clover Collections, Co.                            Ireland                                               189
      10297 16-JUN-22    17359.53               7

Atelier graphique                                  France                                                103
      10298 27-JUN-22     6066.78               2

Marta's Replicas Co.                               USA                                                   286
      10305 13-JUL-22    47411.33              14

Toms Spezialitaten, Ltd                            Germany                                               259
      10310 16-JUL-22    61234.67              17

Euro+ Shopping Channel                             Spain                                                 141
      10311 16-JUL-22    36140.38              11

La Rochelle Gifts                                  France                                                119
      10315 29-JUL-22    19501.82               7

FunGiftIdeas.com                                   USA                                                   462
      10321 04-AUG-22    48355.87              15

Classic Legends Inc.                               USA                                                   424
      10337 21-AUG-22    25505.98               9

Royale Belge                                       Belgium                                               381
      10338 22-AUG-22    12081.52               3

Euro+ Shopping Channel                             Spain                                                 141
      10358 10-SEP-22    44185.46              14

Euro+ Shopping Channel                             Spain                                                 141
      10104 31-OCT-20     40206.2              13

Mini Gifts Distributors Ltd.                       USA                                                   124
      10135 02-APR-21    55601.84              17

Kelly's Gift Shop                                  New Zealand                                           496
      10138 07-APR-21    32077.44              13

CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------

Souveniers And Things Co.                          Australia                                             282
      10139 16-APR-21    24013.52               8

Mini Gifts Distributors Ltd.                       USA                                                   124
      10142 08-MAY-21    56052.56              16

Anna's Decorations, Ltd                            Australia                                             276
      10148 11-JUN-21    41554.73              14

Euro+ Shopping Channel                             Spain                                                 141
      10153 28-JUN-21    44939.85              13

Classic Legends Inc.                               USA                                                   424
      10163 20-JUL-21    22042.37               6

Mini Auto Werke                                    Austria                                               452
      10164 21-JUL-21     27121.9               8

Stylish Desk Decors, Co.                           UK                                                    324
      10175 06-AUG-21    37455.77              12

King Kong Collectables, Co.                        Hong Kong                                             211
      10187 15-AUG-21    28287.73              10

Australian Collectables, Ltd                       Australia                                             471
      10193 21-AUG-21    35505.63              16

Mini Classics                                      USA                                                   319
      10195 25-AUG-21     36092.4              10

Euro+ Shopping Channel                             Spain                                                 141
      10205 03-SEP-21    13059.16               5

Double Decker Gift Stores, Ltd                     UK                                                    489
      10213 22-OCT-21     7310.42               3

Auto Associes & Cie.                               France                                                256
      10216 02-NOV-21     5759.42               1

Frau da Collezione                                 Italy                                                 473
      10218 09-NOV-21     7612.06               2

Petit Auto                                         Belgium                                               314
      10221 18-NOV-21    16901.38               5

Daedalus Designs Imports                           France                                                171
      10224 21-NOV-21    18997.89               6

Technics Stores Inc.                               USA                                                   161
      10362 05-OCT-22    12692.19               4

Oulu Toy Supplies, Inc.                            Finland                                               311
      10373 31-OCT-22    46770.52              17

La Rochelle Gifts                                  France                                                119
      10375 03-NOV-22    49523.67              15

Corporate Gift Ideas Co.                           USA                                                   321
      10381 17-NOV-22    32626.09               9

Corporate Gift Ideas Co.                           USA                                                   321
      10384 23-NOV-22    14155.57               4

Alpha Cognac                                       France                                                242
      10397 28-DEC-22    12432.32               5

Danish Wholesale Imports                           Denmark                                               145
      10406 15-JAN-23    21638.62               3

Gift Depot Inc.                                    USA                                                   175
      10413 05-FEB-23    28500.78               6

Extreme Desk Decorations, Ltd                      New Zealand                                           412
      10418 16-FEB-23    23627.44               9

Diecast Collectables                               USA                                                   495
      10243 26-JAN-22      6276.6               2

Euro+ Shopping Channel                             Spain                                                 141
      10244 29-JAN-22    26155.91               9

Land of Toys Inc.                                  USA                                                   131
      10248 07-FEB-22    41445.21              14

Cambridge Collectables Co.                         USA                                                   173
      10249 08-FEB-22    11843.45               5

The Sharp Gifts Warehouse                          USA                                                   450
      10250 11-FEB-22    42798.08              14

Australian Collectables, Ltd                       Australia                                             471
      10265 02-APR-22     9415.13               2

Online Mini Collectables                           USA                                                   204

CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------
      10276 02-MAY-22    51152.86              14

Mini Gifts Distributors Ltd.                       USA                                                   124
      10282 20-MAY-22    47979.98              13

Bavarian Collectables Imports, Co.                 Germany                                               415
      10296 15-JUN-22    31310.09              14

AV Stores, Co.                                     UK                                                    187
      10306 14-JUL-22    52825.29              17

Classic Gift Ideas, Inc                            USA                                                   339
      10307 14-JUL-22    23333.06               9

Canadian Gift Exchange Network                     Canada                                                202
      10313 22-JUL-22    33594.58              11

Online Diecast Creations Co.                       USA                                                   363
      10322 04-AUG-22    50799.69              14

Australian Collectors, Co.                         Australia                                             114
      10342 24-AUG-22     40265.6              11

Rovelli Gifts                                      Italy                                                 278
      10106 17-NOV-20    52151.81              18

Cruz & Sons Co.                                    Philippines                                           385
      10108 03-DEC-20    51001.22              16

Australian Collectors, Co.                         Australia                                             114
      10120 29-JAN-21    45864.03              15

Australian Collectors, Co.                         Australia                                             114
      10125 21-FEB-21     7565.08               2

Auto-Moto Classics Inc.                            USA                                                   198
      10130 16-MAR-21     6036.96               2

Gift Ideas Corp.                                   USA                                                   447
      10131 16-MAR-21    17032.29               8

Reims Collectables                                 France                                                353
      10137 10-APR-21    13920.26               4

Technics Stores Inc.                               USA                                                   161
      10140 24-APR-21    38675.13              11

Mini Creations Ltd.                                USA                                                   320
      10143 10-MAY-21    41016.75              16

Dragon Souveniers, Ltd.                            Singapore                                             148
      10150 19-JUN-21    38350.15              11

Corporate Gift Ideas Co.                           USA                                                   321
      10162 18-JUL-21    30876.44              10

Dragon Souveniers, Ltd.                            Singapore                                             148
      10165 22-JUL-21    67392.85              18

Kelly's Gift Shop                                  New Zealand                                           496
      10179 11-AUG-21     22963.6               9

Daedalus Designs Imports                           France                                                171
      10180 11-AUG-21    42783.81              14

Saveley & Henriot, Co.                             France                                                146
      10194 25-AUG-21     39712.1              11

Super Scale Inc.                                   USA                                                   455
      10196 26-AUG-21    38139.18               8

Enaco Distributors                                 Spain                                                 216
      10197 26-AUG-21    40473.86              14

Euro+ Shopping Channel                             Spain                                                 141
      10203 02-SEP-21    40062.53              11

Auto Canal+ Petit                                  France                                                406
      10211 15-OCT-21    49165.16              15

Mini Gifts Distributors Ltd.                       USA                                                   124
      10229 11-DEC-21     43369.3              14

Suominen Souveniers                                Finland                                               334
      10363 06-OCT-22    45785.34              15

Mini Gifts Distributors Ltd.                       USA                                                   124
      10371 23-OCT-22    35137.54              12

Mini Gifts Distributors Ltd.                       USA                                                   124
      10382 17-NOV-22    47765.59              13

Mini Auto Werke                                    Austria                                               452
      10392 10-DEC-22     8807.12               3


CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------
Mini Gifts Distributors Ltd.                       USA                                                   124
      10396 23-DEC-22    27695.54               8

Reims Collectables                                 France                                                353
      10398 30-DEC-22    46656.94              18

Kelly's Gift Shop                                  New Zealand                                           496
      10399 01-JAN-23    30253.75               8

UK Collectables, Ltd.                              UK                                                    201
      10403 08-JAN-23    37258.94               9

The Sharp Gifts Warehouse                          USA                                                   450
      10407 22-JAN-23    52229.55              12

La Rochelle Gifts                                  France                                                119
      10425 28-FEB-23    41623.44              13

Royal Canadian Collectables, Ltd.                  Canada                                                260
      10235 02-JAN-22    29284.42              12

Down Under Souveniers, Inc                         New Zealand                                           323
      10254 03-MAR-22    37281.36              13

Tokyo Collectables, Ltd                            Japan                                                 398
      10258 15-MAR-22    22037.91               6

Diecast Classics Inc.                              USA                                                   157
      10281 19-MAY-22    39641.43              14

Scandinavian Gift Ideas                            Sweden                                                448
      10291 08-JUN-22     48809.9              14

Land of Toys Inc.                                  USA                                                   131
      10292 08-JUN-22    35321.97              12

Amica Models & Co.                                 Italy                                                 249
      10293 09-JUN-22    33924.24               9

Toys of Finland, Co.                               Finland                                               186
      10299 30-JUN-22    34341.08              11

UK Collectables, Ltd.                              UK                                                    201
      10302 06-JUL-21    23908.24               6

Baane Mini Imports                                 Norway                                                121
      10309 15-JUL-22    17876.32               6

Microscale Inc.                                    USA                                                   456
      10319 03-AUG-22    27550.51               9

Blauer See Auto, Co.                               Germany                                               128
      10323 05-AUG-22     7466.32               2

Vitachrome Inc.                                    USA                                                   181
      10324 05-AUG-22     44400.5              14

Danish Wholesale Imports                           Denmark                                               145
      10327 10-AUG-22    20564.86               8

Enaco Distributors                                 Spain                                                 216
      10340 24-AUG-22    24945.21               8

Stylish Desk Decors, Co.                           UK                                                    324
      10351 03-SEP-22    13671.82               5

Lyon Souveniers                                    France                                                250
      10356 09-SEP-22    26311.63               9

Mini Gifts Distributors Ltd.                       USA                                                   124
      10357 10-SEP-22    40676.26              10

Kelly's Gift Shop                                  New Zealand                                           496
      10360 16-SEP-22       52166              18

Blauer See Auto, Co.                               Germany                                               128
      10101 09-OCT-20    10549.01               4

Land of Toys Inc.                                  USA                                                   131
      10107 24-NOV-20    22292.62               8

AV Stores, Co.                                     UK                                                    187
      10110 18-DEC-20    48425.69              16

Reims Collectables                                 France                                                353
      10121 07-FEB-21    16700.47               5

Collectables For Less Inc.                         USA                                                   379
      10147 05-JUN-21    32680.31              11

Technics Stores Inc.                               USA                                                   161
      10168 28-JUL-21    50743.65              18

Australian Gift Network, Co                        Australia                                             333
      10174 06-AUG-21    23936.53               5

CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------

Online Diecast Creations Co.                       USA                                                   363
      10192 20-AUG-21    55425.77              16

GiftsForHim.com                                    New Zealand                                           357
      10202 02-SEP-21    20220.04               7

Muscle Machine Inc                                 USA                                                   151
      10204 02-SEP-21    58793.53              17

Diecast Collectables                               USA                                                   495
      10207 09-SEP-21    59265.14              16

Euro+ Shopping Channel                             Spain                                                 141
      10212 16-OCT-21    59830.55              16

Signal Collectibles Ltd.                           USA                                                   487
      10219 10-NOV-21    12573.28               4

Collectable Mini Designs Co.                       USA                                                   239
      10222 19-NOV-21    56822.65              18

Cambridge Collectables Co.                         USA                                                   173
      10228 10-DEC-21    20355.24               6

Souveniers And Things Co.                          Australia                                             282
      10361 17-SEP-22    31835.36              14

Toys4GrownUps.com                                  USA                                                   205
      10367 12-OCT-22     39580.6              13

Euro+ Shopping Channel                             Spain                                                 141
      10394 15-DEC-22    18102.74               7

Tekni Collectables Inc.                            USA                                                   328
      10401 03-JAN-23    43525.04              12

Osaka Souveniers Co.                               Japan                                                 177
      10240 13-JAN-22    15183.63               3

Euro+ Shopping Channel                             Spain                                                 141
      10246 05-FEB-22    35420.74              11

Auto Canal+ Petit                                  France                                                406
      10252 26-FEB-22    25080.96               9

Extreme Desk Decorations, Ltd                      New Zealand                                           412
      10268 12-APR-22    35034.57              11

Diecast Classics Inc.                              USA                                                   157
      10272 20-APR-22     23715.7               6

Norway Gifts By Mail, Co.                          Norway                                                299
      10284 21-MAY-22    32260.16              13

Blauer See Auto, Co.                               Germany                                               128
      10300 04-JUL-21    24101.81               8

Volvo Model Replicas, Co                           Sweden                                                144
      10320 03-AUG-22    16799.03               5

AV Stores, Co.                                     UK                                                    187
      10332 17-AUG-22    47159.11              18

Tokyo Collectables, Ltd                            Japan                                                 398
      10339 23-AUG-22    48927.64              16

Australian Collectors, Co.                         Australia                                             114
      10347 29-AUG-22    41995.62              12

Baane Mini Imports                                 Norway                                                121
      10103 29-OCT-20    50218.95              16

Motor Mint Distributors Inc.                       USA                                                   486
      10109 10-DEC-20    25833.14               6

Dragon Souveniers, Ltd.                            Singapore                                             148
      10117 16-JAN-21    44380.15              12

Atelier graphique                                  France                                                103
      10123 20-FEB-21    14571.44               4

Muscle Machine Inc                                 USA                                                   151
      10127 03-MAR-21    58841.35              15

Euro+ Shopping Channel                             Spain                                                 141
      10128 06-MAR-21    13884.99               4

Oulu Toy Supplies, Inc.                            Finland                                               311
      10151 21-JUN-21    32723.04              10

Frau da Collezione                                 Italy                                                 473
      10157 09-JUL-21    17746.26               6

FunGiftIdeas.com                                   USA                                                   462

CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------
      10166 21-JUL-21     9977.85               3

Gift Depot Inc.                                    USA                                                   175
      10172 05-AUG-21    24879.08               8

Classic Gift Ideas, Inc                            USA                                                   339
      10183 13-AUG-21    34606.28              12

Herkku Gifts                                       Norway                                                167
      10188 18-AUG-21    29954.91               8

Cruz & Sons Co.                                    Philippines                                           385
      10198 27-AUG-21    20644.24               6

West Coast Collectables Co.                        USA                                                   475
      10215 29-OCT-21    36070.47              10

Handji Gifts& Co                                   Singapore                                             166
      10217 04-NOV-21    22474.17               7

Clover Collections, Co.                            Ireland                                               189
      10220 12-NOV-21    32538.74               9

Anna's Decorations, Ltd                            Australia                                             276
      10370 20-OCT-22    27083.78               9

Toys of Finland, Co.                               Finland                                               186
      10377 09-NOV-22     23602.9               6

The Sharp Gifts Warehouse                          USA                                                   450
      10400 01-JAN-23    31755.34               9

Petit Auto                                         Belgium                                               314
      10423 28-FEB-23     8597.73               5

giftsbymail.co.uk                                  UK                                                    240
      10232 20-DEC-21    24995.61               8

Danish Wholesale Imports                           Denmark                                               145
      10238 09-JAN-22     28211.7               8

Mini Caravy                                        France                                                209
      10241 13-JAN-22    36069.26              12

Suominen Souveniers                                Finland                                               334
      10247 05-FEB-22    28394.54               6

The Sharp Gifts Warehouse                          USA                                                   450
      10257 14-MAR-22     16753.3               5

Gift Depot Inc.                                    USA                                                   175
      10263 28-MAR-22    42044.77              11

Petit Auto                                         Belgium                                               314
      10273 21-APR-22    45352.47              15

La Rochelle Gifts                                  France                                                119
      10275 23-APR-22    47924.19              18

Norway Gifts By Mail, Co.                          Norway                                                299
      10301 05-JUL-21    36798.88              11

Auto Associes & Cie.                               France                                                256
      10304 11-JUL-22    53116.99              17

Volvo Model Replicas, Co                           Sweden                                                144
      10334 19-AUG-22    23014.17               6

Mini Gifts Distributors Ltd.                       USA                                                   124
      10335 19-AUG-22     6466.44               3

La Corne D'abondance, Co.                          France                                                172
      10336 20-AUG-22    51209.58              12

Reims Collectables                                 France                                                353
      10343 24-AUG-22    17104.91               6

Signal Gift Stores                                 USA                                                   112
      10346 29-AUG-22    14191.12               6

Corrida Auto Replicas, Ltd                         Spain                                                 458
      10348 01-AUG-22    33145.56               8

Euro+ Shopping Channel                             Spain                                                 141
      10355 07-SEP-22    25529.78              10

Online Diecast Creations Co.                       USA                                                   363
      10100 06-OCT-20    10223.83               4

Classic Legends Inc.                               USA                                                   424
      10115 04-JAN-21    21665.98               5

Marseille Mini Autos                               France                                                350
      10122 08-FEB-21    50824.66              17


CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------
Signal Gift Stores                                 USA                                                   112
      10124 21-FEB-21    32641.98              13

Corrida Auto Replicas, Ltd                         Spain                                                 458
      10126 28-FEB-21    57131.92              17

Lyon Souveniers                                    France                                                250
      10134 01-APR-21    23419.47               7

Alpha Cognac                                       France                                                242
      10136 04-APR-21     14232.7               3

Suominen Souveniers                                Finland                                               334
      10141 01-MAY-21    29716.86               9

Gift Ideas Corp.                                   USA                                                   447
      10146 03-JUN-21     6631.36               2

Signal Collectibles Ltd.                           USA                                                   487
      10149 12-JUN-21    29997.09              11

Corporate Gift Ideas Co.                           USA                                                   321
      10159 10-JUL-21    54682.68              18

Heintze Collectables                               Denmark                                               227
      10161 17-JUL-21    36164.46              12

Rovelli Gifts                                      Italy                                                 278
      10173 05-AUG-21    37723.79              16

CAF Imports                                        Spain                                                 344
      10177 07-AUG-21    31428.21              11

West Coast Collectables Co.                        USA                                                   475
      10199 01-SEP-21     7678.25               3

Mini Wheels Co.                                    USA                                                   129
      10201 01-SEP-21    23923.93               7

Men 'R' US Retailers, Ltd.                         USA                                                   347
      10209 09-OCT-21    21053.69               8

Osaka Souveniers Co.                               Japan                                                 177
      10210 12-OCT-21    47177.59              17

Vida Sport, Ltd                                    Switzerland                                           298
      10225 22-NOV-21    47375.92              14

Mini Creations Ltd.                                USA                                                   320
      10365 07-OCT-22     8307.28               3

Mini Gifts Distributors Ltd.                       USA                                                   124
      10368 19-OCT-22    13874.75               5

Euro+ Shopping Channel                             Spain                                                 141
      10378 10-NOV-22    32289.12              10

Euro+ Shopping Channel                             Spain                                                 141
      10383 22-NOV-22    36851.98              13

Euro+ Shopping Channel                             Spain                                                 141
      10386 01-DEC-22    46968.52              18

Scandinavian Gift Ideas                            Sweden                                                448
      10389 03-DEC-22    27966.54               8

Down Under Souveniers, Inc                         New Zealand                                           323
      10393 11-DEC-22    33593.32              11

Down Under Souveniers, Inc                         New Zealand                                           323
      10404 08-JAN-23    41426.81               8

L'ordine Souveniers                                Italy                                                 386
      10416 10-FEB-23    35362.26              14

Extreme Desk Decorations, Ltd                      New Zealand                                           412
      10234 30-DEC-21    31670.37               9

Motor Mint Distributors Inc.                       USA                                                   486
      10236 03-JAN-22     5899.38               3

Euro+ Shopping Channel                             Spain                                                 141
      10262 24-MAR-22    47065.36              16

Souveniers And Things Co.                          Australia                                             282
      10270 19-APR-22    35806.73              11

Signal Gift Stores                                 USA                                                   112
      10278 06-MAY-22    33347.88              10

Royal Canadian Collectables, Ltd.                  Canada                                                260
      10283 20-MAY-22    37527.58              14

Handji Gifts& Co                                   Singapore                                             166
      10288 01-JUN-22    38785.48              14

CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------

Mini Classics                                      USA                                                   319
      10308 15-JUL-22    42339.76              16

Heintze Collectables                               Denmark                                               227
      10314 22-JUL-22    53745.34              15

Rovelli Gifts                                      Italy                                                 278
      10328 12-AUG-22    37654.09              14

Cruz & Sons Co.                                    Philippines                                           385
      10330 16-AUG-22    15822.84               4

Mini Wheels Co.                                    USA                                                   129
      10333 18-AUG-22    26248.78               8

Salzburg Collectables                              Austria                                               382
      10341 24-AUG-22    42813.83              10

Marseille Mini Autos                               France                                                350
      10344 25-AUG-22    18888.31               7

Muscle Machine Inc                                 USA                                                   151
      10349 01-SEP-22    39964.63              10

Gift Ideas Corp.                                   USA                                                   447
      10353 04-SEP-22    26304.13               9

Danish Wholesale Imports                           Denmark                                               145
      10105 11-NOV-20    53959.21              15

Mini Wheels Co.                                    USA                                                   129
      10111 25-DEC-20    16537.85               6

Salzburg Collectables                              Austria                                               382
      10119 28-JAN-21    35826.33              14

Australian Gift Network, Co                        Australia                                             333
      10152 25-JUN-21     9821.32               4

Toys of Finland, Co.                               Finland                                               186
      10155 06-JUL-21    37602.48              13

Quebec Home Shopping Network                       Canada                                                233
      10171 05-AUG-21    16909.84               4

L'ordine Souveniers                                Italy                                                 386
      10176 06-AUG-21    38524.29              10

Mini Gifts Distributors Ltd.                       USA                                                   124
      10182 12-AUG-21    45084.38              17

Euro+ Shopping Channel                             Spain                                                 141
      10190 19-AUG-21    10721.86               4

Toms Spezialitaten, Ltd                            Germany                                               259
      10191 20-AUG-21    27988.47               9

Saveley & Henriot, Co.                             France                                                146
      10227 02-DEC-21    40978.53              15

Euro+ Shopping Channel                             Spain                                                 141
      10379 10-NOV-22    16621.27               5

FunGiftIdeas.com                                   USA                                                   462
      10388 03-DEC-22    30293.77               8

Mini Gifts Distributors Ltd.                       USA                                                   124
      10390 04-DEC-22     55902.5              16

Anna's Decorations, Ltd                            Australia                                             276
      10391 09-DEC-22    29848.52              10

Auto Canal+ Petit                                  France                                                406
      10402 07-JAN-23    12190.85               3

GiftsForHim.com                                    New Zealand                                           357
      10410 29-JAN-23    36442.34               9

Euro+ Shopping Channel                             Spain                                                 141
      10412 03-FEB-23    46895.48              11

Australian Collectables, Ltd                       Australia                                             471
      10415 09-FEB-23    10945.26               5

Euro+ Shopping Channel                             Spain                                                 141
      10417 13-FEB-23     28574.9               6

Souveniers And Things Co.                          Australia                                             282
      10420 28-FEB-23    42251.51              13

Oulu Toy Supplies, Inc.                            Finland                                               311
      10239 12-JAN-22    16212.59               5

Super Scale Inc.                                   USA                                                   455

CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------
      10245 04-FEB-22    32239.47               9

Quebec Home Shopping Network                       Canada                                                233
      10261 17-MAR-22    22997.45               9

Salzburg Collectables                              Austria                                               382
      10269 16-APR-22     6419.84               2

Euro+ Shopping Channel                             Spain                                                 141
      10279 09-MAY-22    20009.53               6

Marta's Replicas Co.                               USA                                                   286
      10285 27-MAY-22    43134.04              13

Vida Sport, Ltd                                    Switzerland                                           298
      10287 30-MAY-22       61402              17

Gifts4AllAges.com                                  USA                                                   362
      10295 10-JUN-22    15059.76               5

giftsbymail.co.uk                                  UK                                                    240
      10316 01-AUG-22    46788.14              18

Land of Toys Inc.                                  USA                                                   131
      10329 15-AUG-22    50025.35              15

Euro+ Shopping Channel                             Spain                                                 141
      10350 02-SEP-22    46493.16              17

Mini Gifts Distributors Ltd.                       USA                                                   124
      10113 26-DEC-20     11044.3               4

Euro+ Shopping Channel                             Spain                                                 141
      10133 27-MAR-21    22366.04               8

Scandinavian Gift Ideas                            Sweden                                                448
      10167 23-JUL-21    44167.09              16

Anna's Decorations, Ltd                            Australia                                             276
      10169 04-AUG-21    38547.19              13

Alpha Cognac                                       France                                                242
      10178 08-AUG-21    33818.34              12

Herkku Gifts                                       Norway                                                167
      10181 12-AUG-21    55069.55              17

Iberia Gift Imports, Corp.                         Spain                                                 484
      10184 14-AUG-21    47513.19              13

Mini Creations Ltd.                                USA                                                   320
      10185 14-AUG-21    52548.49              16

Saveley & Henriot, Co.                             France                                                146
      10208 02-OCT-21    49614.72              15

Corrida Auto Replicas, Ltd                         Spain                                                 458
      10214 26-OCT-21    22162.61               7

Collectable Mini Designs Co.                       USA                                                   239
      10226 26-NOV-21    23552.59               7

Euro+ Shopping Channel                             Spain                                                 141
      10380 16-NOV-22    34404.21              13

Lyon Souveniers                                    France                                                250
      10395 17-DEC-22    17928.09               4

Mini Caravy                                        France                                                209
      10405 14-JAN-23    35157.75               5

Quebec Home Shopping Network                       Canada                                                233
      10411 01-FEB-23    29070.38               9

Gifts4AllAges.com                                  USA                                                   362
      10414 06-FEB-23    50806.85              14

Diecast Classics Inc.                              USA                                                   157
      10422 28-FEB-23     5849.44               2

Euro+ Shopping Channel                             Spain                                                 141
      10424 28-FEB-23     29310.3               6

CAF Imports                                        Spain                                                 344
      10231 19-DEC-21    15322.93               2

Gifts4AllAges.com                                  USA                                                   362
      10264 31-MAR-22    18473.71               7

Muscle Machine Inc                                 USA                                                   151
      10267 07-APR-22    20314.44               6

Mini Gifts Distributors Ltd.                       USA                                                   124
      10271 20-APR-22    37430.89              11


CUSTOMERNAME                                       COUNTRY                                            CUSTOMERNUMBER
-------------------------------------------------- -------------------------------------------------- --------------
ORDERNUMBER ORDERDATE Order Value No. of Products
----------- --------- ----------- ---------------
Collectables For Less Inc.                         USA                                                   379
      10274 21-APR-22    12530.51               5

Amica Models & Co.                                 Italy                                                 249
      10280 17-MAY-22    48298.99              17

Auto-Moto Classics Inc.                            USA                                                   198
      10290 07-JUN-22     5858.56               2

Mini Gifts Distributors Ltd.                       USA                                                   124
      10312 21-JUL-22    55639.66              17

Diecast Classics Inc.                              USA                                                   157
      10318 02-AUG-22    35152.12               9

Baane Mini Imports                                 Norway                                                121
      10325 05-AUG-22    34638.14               9

Volvo Model Replicas, Co                           Sweden                                                144
      10326 09-AUG-22    19206.68               6

Motor Mint Distributors Inc.                       USA                                                   486
      10331 17-AUG-22    45994.07              14

Auto-Moto Classics Inc.                            USA                                                   198
      10352 03-SEP-22     9658.74               4

Down Under Souveniers, Inc                         New Zealand                                           323
      10354 04-SEP-22    39440.59              13

Reims Collectables                                 France                                                353
      10359 15-SEP-22    32600.61               8

Vitachrome Inc.                                    USA                                                   181
      10102 10-OCT-20     5494.78               2

Volvo Model Replicas, Co                           Sweden                                                144
      10112 24-DEC-20     7674.94               2

La Corne D'abondance, Co.                          France                                                172
      10114 01-JAN-21    33383.14              10

Stylish Desk Decors, Co.                           UK                                                    324
      10129 12-MAR-21    29429.14               9

Toys4GrownUps.com                                  USA                                                   205
      10145 25-MAY-21    50342.74              16

Men 'R' US Retailers, Ltd.                         USA                                                   347
      10160 11-JUL-21     20452.5               6

Mini Auto Werke                                    Austria                                               452
      10170 04-AUG-21    15130.97               4


303 rows selected.
*/

--Other Data Dictionary: user_constraints, user_tables, user_views, user_source
SELECT OWNER, CONSTRAINT_NAME, CONSTRAINT_TYPE, TABLE_NAME, STATUS
FROM user_constraints 
ORDER BY owner, table_name;

/*
OWNER
------------------------------------------------------------------------------------------------------------------------
CONSTRAINT_NAME                C TABLE_NAME                     STATUS
------------------------------ - ------------------------------ --------
MONG2N05
BIN$5yPAHBYZTHeFp6MnRQuueg==$0 C BIN$ELpS5iZ1T+Wg0vjFoD47EQ==$0 ENABLED

MONG2N05
BIN$s6kkIw8uQUeatuUBhB3f4g==$0 P BIN$ELpS5iZ1T+Wg0vjFoD47EQ==$0 ENABLED

MONG2N05
BIN$8X+cNOK0Q9y5lOT9T7v3pg==$0 C BIN$ELpS5iZ1T+Wg0vjFoD47EQ==$0 ENABLED

MONG2N05
BIN$vg6yV4hoS5mE5mxv0cYKMg==$0 P BIN$NyrGT6q0QMW5FvU9vyA91A==$0 ENABLED

MONG2N05
BIN$pedWz/AGSNu6iMGnRSO7kg==$0 C BIN$NyrGT6q0QMW5FvU9vyA91A==$0 ENABLED

MONG2N05
BIN$kzRsah6LQp+hHrq8kVo0AA==$0 C BIN$hKQGBWi2SkO156T35v43yg==$0 ENABLED

MONG2N05
BIN$YVJbK42XRFW4syfVfDN13w==$0 P BIN$hKQGBWi2SkO156T35v43yg==$0 ENABLED

MONG2N05
BIN$aWvoitzvSGiTYjZ0M8Y9Lg==$0 C BIN$hmdii8vwS4GZzSRtnZ2ntw==$0 ENABLED

MONG2N05
BIN$BGyNGsLrS5q12ot9heQCIg==$0 P BIN$hmdii8vwS4GZzSRtnZ2ntw==$0 ENABLED

MONG2N05
BIN$sDldarh0RoqR40Z9xlpNmQ==$0 P BIN$mL1D3TizRWetlV4uOBkzEw==$0 ENABLED

MONG2N05
BIN$pFTcN/nvQ1igu6Val7Hlog==$0 C BIN$mL1D3TizRWetlV4uOBkzEw==$0 ENABLED

MONG2N05
SYS_C00123164                  P BRANCH                         ENABLED

MONG2N05
SYS_C00123163                  C BRANCH                         ENABLED

MONG2N05
SYS_C0092720                   C CUSTOMERS                      ENABLED

MONG2N05
SYS_C0092721                   C CUSTOMERS                      ENABLED

MONG2N05
SYS_C0092722                   C CUSTOMERS                      ENABLED

MONG2N05
SYS_C0092723                   C CUSTOMERS                      ENABLED

MONG2N05
SYS_C0092724                   C CUSTOMERS                      ENABLED

MONG2N05
SYS_C0092725                   C CUSTOMERS                      ENABLED

MONG2N05
SYS_C0092728                   P CUSTOMERS                      ENABLED

MONG2N05
SYS_C0092727                   C CUSTOMERS                      ENABLED

MONG2N05
SYS_C0092726                   C CUSTOMERS                      ENABLED

MONG2N05
SYS_C00144412                  C CUSTOMERS1                     ENABLED

MONG2N05
SYS_C00144413                  C CUSTOMERS1                     ENABLED

MONG2N05
SYS_C00144414                  P CUSTOMERS1                     ENABLED

MONG2N05
SYS_C00144466                  C DELETECUSTOMERS1               ENABLED

MONG2N05
SYS_C00144465                  C DELETECUSTOMERS1               ENABLED

MONG2N05
SYS_C0092716                   C EMPLOYEES                      ENABLED

MONG2N05
SYS_C0092719                   P EMPLOYEES                      ENABLED

MONG2N05
SYS_C0092718                   C EMPLOYEES                      ENABLED

MONG2N05
SYS_C0092717                   C EMPLOYEES                      ENABLED

MONG2N05
SYS_C00144461                  C INSERTCUSTOMERS1               ENABLED

OWNER
------------------------------------------------------------------------------------------------------------------------
CONSTRAINT_NAME                C TABLE_NAME                     STATUS
------------------------------ - ------------------------------ --------

MONG2N05
SYS_C00144460                  C INSERTCUSTOMERS1               ENABLED

MONG2N05
SYS_C00123171                  C JOBDONE                        ENABLED

MONG2N05
SYS_C00123174                  R JOBDONE                        ENABLED

MONG2N05
SYS_C00123173                  R JOBDONE                        ENABLED

MONG2N05
SYS_C00123172                  P JOBDONE                        ENABLED

MONG2N05
SYS_C00123167                  C MECHANIC                       ENABLED

MONG2N05
SYS_C00123170                  R MECHANIC                       ENABLED

MONG2N05
SYS_C00123169                  P MECHANIC                       ENABLED

MONG2N05
SYS_C00123168                  C MECHANIC                       ENABLED

MONG2N05
SYS_C0092708                   C OFFICES                        ENABLED

MONG2N05
SYS_C0092709                   C OFFICES                        ENABLED

MONG2N05
SYS_C0092710                   C OFFICES                        ENABLED

MONG2N05
SYS_C0092711                   C OFFICES                        ENABLED

MONG2N05
SYS_C0092712                   C OFFICES                        ENABLED

MONG2N05
SYS_C0092713                   C OFFICES                        ENABLED

MONG2N05
SYS_C0092714                   C OFFICES                        ENABLED

MONG2N05
SYS_C0092715                   P OFFICES                        ENABLED

MONG2N05
SYS_C0092749                   C ORDERDETAILS                   ENABLED

MONG2N05
SYS_C0092750                   P ORDERDETAILS                   ENABLED

MONG2N05
SYS_C0092748                   C ORDERDETAILS                   ENABLED

MONG2N05
SYS_C0092747                   C ORDERDETAILS                   ENABLED

MONG2N05
SYS_C0092746                   C ORDERDETAILS                   ENABLED

MONG2N05
SYS_C0092745                   C ORDERDETAILS                   ENABLED

MONG2N05
SYS_C0092734                   P ORDERS                         ENABLED

MONG2N05
SYS_C0092733                   C ORDERS                         ENABLED

MONG2N05
SYS_C0092732                   C ORDERS                         ENABLED

MONG2N05
SYS_C0092731                   C ORDERS                         ENABLED

MONG2N05
SYS_C0092730                   C ORDERS                         ENABLED

MONG2N05
SYS_C0092729                   C ORDERS                         ENABLED

MONG2N05
SYS_C0092706                   C PRODUCTLINES                   ENABLED

MONG2N05
SYS_C0092707                   P PRODUCTLINES                   ENABLED

MONG2N05

OWNER
------------------------------------------------------------------------------------------------------------------------
CONSTRAINT_NAME                C TABLE_NAME                     STATUS
------------------------------ - ------------------------------ --------
SYS_C0092737                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C0092744                   P PRODUCTS                       ENABLED

MONG2N05
SYS_C0092739                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C0092740                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C0092741                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C0092742                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C0092743                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C0092736                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C0092735                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C0092738                   C PRODUCTS                       ENABLED

MONG2N05
SYS_C00144408                  C PRODUCTS1                      ENABLED

MONG2N05
SYS_C00144409                  C PRODUCTS1                      ENABLED

MONG2N05
SYS_C00144410                  C PRODUCTS1                      ENABLED

MONG2N05
SYS_C00144411                  P PRODUCTS1                      ENABLED

MONG2N05
SYS_C00144415                  C SALES1                         ENABLED

MONG2N05
SYS_C00144416                  C SALES1                         ENABLED

MONG2N05
SYS_C00144417                  C SALES1                         ENABLED

MONG2N05
SYS_C00144418                  C SALES1                         ENABLED

MONG2N05
SYS_C00144419                  C SALES1                         ENABLED

MONG2N05
SYS_C00144420                  P SALES1                         ENABLED

MONG2N05
SYS_C00144421                  R SALES1                         ENABLED

MONG2N05
SYS_C00144422                  R SALES1                         ENABLED

MONG2N05
SIMPLE_OFFICE1_READONLY        O SIMPLEOFFICE1STAFF             ENABLED

MONG2N05
SYS_C00144462                  C UPDATECUSTOMERS1               ENABLED

MONG2N05
SYS_C00144463                  C UPDATECUSTOMERS1               ENABLED

MONG2N05
SYS_C00144464                  C UPDATECUSTOMERS1               ENABLED

MONG2N05
SYS_C00123166                  P VEHICLE                        ENABLED

MONG2N05
SYS_C00123165                  C VEHICLE                        ENABLED


91 rows selected.
*/

SELECT table_name
FROM user_tables;

/*
TABLE_NAME
------------------------------
VEHICLE
UPDATECUSTOMERS1
SALES1
PRODUCTS_AUDIT
PRODUCTS1_AUDIT
PRODUCTS1
PRODUCTS
PRODUCTLINES
ORDERS
ORDERDETAILS
OFFICES
MECHANIC
JOBDONE
INSERTCUSTOMERS1
EMPLOYEES
DELETECUSTOMERS1
CUSTOMERS1
CUSTOMERS
BRANCH

19 rows selected.
*/

SELECT *
FROM user_views;

/*
VIEW_NAME                      TEXT_LENGTH
------------------------------ -----------
TEXT                                                                             TYPE_TEXT_LENGTH
-------------------------------------------------------------------------------- ----------------
TYPE_TEXT
------------------------------------------------------------------------------------------------------------------------
OID_TEXT_LENGTH
---------------
OID_TEXT
------------------------------------------------------------------------------------------------------------------------
VIEW_TYPE_OWNER                VIEW_TYPE                      SUPERVIEW_NAME                 E R
------------------------------ ------------------------------ ------------------------------ - -
SIMPLEOFFICE1STAFF                     116
SELECT EmployeeNumber, LastName, FirstName, Email, officeCode
FROM Office1Staff



                                                                                             N Y

OFFICE1STAFF                           117
SELECT "EMPLOYEENUMBER","LASTNAME","FIRSTNAME","EXTENSION","EMAIL","OFFICECODE",



                                                                                             N N

HIGH_ORDER_VIEW                        303
SELECT customerNumber, O.orderNumber, orderDate, sum(priceEach * quantityOrdered



                                                                                             N N
*/

SELECT line, text
FROM user_source
WHERE name = 'TRG_CAL_SST_SELLINGPRICE';

/*
      LINE     TEXT
----------     ------------------------------------------------------
         1     TRIGGER trg_cal_sst_sellingPrice
         2     BEFORE INSERT OR UPDATE OF ProductPrice ON Products1
         3     FOR EACH ROW
         4
         5     BEGIN
         6     :NEW.SST := :NEW.ProductPrice * 0.06;
         7     :NEW.SellingPrice := :NEW.ProductPrice * 1.06;
         8     END;

8 rows selected.
*/