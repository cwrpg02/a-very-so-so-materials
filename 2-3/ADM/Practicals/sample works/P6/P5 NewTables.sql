DROP TABLE Sales1;
DROP TABLE Products1;
DROP TABLE Customers1;
DROP TABLE Products1_Audit;

CREATE TABLE Products1(
ProductCode		VARCHAR(15) NOT NULL,
ProductName		VARCHAR(30) NOT NULL,
ProductPrice	        NUMBER(7,2) NOT NULL,
SST			NUMBER(7,2),
SellingPrice		NUMBER(7,2),
QuantityInStock		NUMBER(5),
PRIMARY KEY (productCode)
);

CREATE TABLE Customers1(
CustID	  CHAR(6) NOT NULL,
CustName  VARCHAR(30) NOT NULL,
CustCreditLimit NUMBER(6,2) default 1000.00,
CustCreditAmount NUMBER(6,2) default 0.00,
PRIMARY KEY (CustID)
);

CREATE TABLE Sales1(
SalesNo     	VARCHAR(15) NOT NULL,
SalesDate   	DATE NOT NULL,
ProductCode 	VARCHAR(15) NOT NULL,
Quantity    	NUMBER(5) NOT NULL,
ActualPrice    	NUMBER(7,2),
Subtotal    	NUMBER(7,2),
CustID	  	CHAR(6) NOT NULL,
PRIMARY KEY (SalesNo),
FOREIGN KEY (ProductCode) REFERENCES Products1(ProductCode),
FOREIGN KEY (CustID) REFERENCES Customers1(CustID)
);

CREATE TABLE Products1_Audit(
 UserID         VARCHAR(30),   
 TransDate      DATE,
 Time           VARCHAR(8),
 Action		VARCHAR(10)
);

INSERT INTO Customers1
VALUES('C00001', 'Eric Heng', 1500, 0);

INSERT INTO Customers1
VALUES('C00002', 'Alice Ooi', 1000, 0);

