SET linesize 120
SET pagesize 100

desc products1

/*
 Name                                                              Null?    Type
 ----------------------------------------------------------------- -------- --------------------------------------------
 PRODUCTCODE                                                       NOT NULL VARCHAR2(15)
 PRODUCTNAME                                                       NOT NULL VARCHAR2(30)
 PRODUCTPRICE                                                      NOT NULL NUMBER(7,2)
 SST                                                                        NUMBER(7,2)
 SELLINGPRICE                                                               NUMBER(7,2)
 QUANTITYINSTOCK                                                            NUMBER(5)

PROCEDURE - statements, ask computer to execute for you; need to manually execute

TRIGGER - automatically run/calculate; no need to say EXEC (execute)
	action: BEFORE
*/

CREATE OR REPLACE TRIGGER trg_cal_sst_sellingPrice
BEFORE INSERT ON Products1
FOR EACH ROW

BEGIN
 :NEW.SST := :NEW.ProductPrice * 0.06;
 :NEW.SellingPrice := :NEW.ProductPrice * 1.06;
END;
/

INSERT INTO Products1 VALUES ('P0001', 'Golden Finger', 100, NULL, NULL, 50);
SELECT * FROM Products1;

/*
PRODUCTCODE     PRODUCTNAME                    PRODUCTPRICE        SST SELLINGPRICE QUANTITYINSTOCK
--------------- ------------------------------ ------------ ---------- ------------ ---------------
P0001           Golden Finger                           100          6          106              50
*/

UPDATE Products1
SET ProductPrice = 120
WHERE ProductCode = 'P0001';

/*
PRODUCTCODE     PRODUCTNAME                    PRODUCTPRICE        SST SELLINGPRICE QUANTITYINSTOCK
--------------- ------------------------------ ------------ ---------- ------------ ---------------
P0001           Golden Finger                           120          6          106              50
*/

CREATE OR REPLACE TRIGGER trg_cal_sst_sellingPrice
BEFORE INSERT OR UPDATE OF ProductPrice ON Products1
FOR EACH ROW

BEGIN
 :NEW.SST := :NEW.ProductPrice * 0.06;
 :NEW.SellingPrice := :NEW.ProductPrice * 1.06;
END;
/

--update again

/*
PRODUCTCODE     PRODUCTNAME                    PRODUCTPRICE        SST SELLINGPRICE QUANTITYINSTOCK
--------------- ------------------------------ ------------ ---------- ------------ ---------------
P0001           Golden Finger                           120        7.2        127.2              50
*/

CREATE OR REPLACE TRIGGER trg_NewSales
BEFORE INSERT ON Sales1
FOR EACH ROW

DECLARE
v_sellingPrice Products1.SellingPrice%TYPE;
v_qtyInStock   Products1.QuantityInStock%TYPE;

BEGIN
 SELECT sellingPrice, QuantityInStock INTO v_sellingPrice, v_qtyInStock
 FROM Products1
 WHERE ProductCode = :NEW.ProductCode;

 IF (:NEW.Quantity <= v_qtyInStock) THEN
  :NEW.ActualPrice := v_sellingPrice;
  :NEW.Subtotal := :NEW.ActualPrice * :NEW.Quantity;

  UPDATE Products1
  SET QuantityInStock = QuantityInStock - :NEW.Quantity
  WHERE ProductCode = :NEW.ProductCode;

  UPDATE Customers1
  SET CustCreditAmount = CustCreditAmount + :NEW.Subtotal
  WHERE CustID = :NEW.CustID;

 ELSE
  RAISE_APPLICATION_ERROR(-20000, 'Your order quantity uis over the quantity in stock: ' || v_qtyInStock);
 END IF;
END;
/

/*
desc sales1
 Name                                                              Null?    Type
 ----------------------------------------------------------------- -------- --------------------------------------------
 SALESNO                                                           NOT NULL VARCHAR2(15)
 SALESDATE                                                         NOT NULL DATE
 PRODUCTCODE                                                       NOT NULL VARCHAR2(15)
 QUANTITY                                                          NOT NULL NUMBER(5)
 ACTUALPRICE                                                                NUMBER(7,2)
 SUBTOTAL                                                                   NUMBER(7,2)
 CUSTID                                                            NOT NULL CHAR(6)
*/

INSERT INTO Sales1 VALUES ('S0001', SYSDATE, 'P0001', 5, NULL, NULL, 'C00001');

/*
SALESNO         SALESDATE PRODUCTCODE       QUANTITY ACTUALPRICE   SUBTOTAL CUSTID
--------------- --------- --------------- ---------- ----------- ---------- ------
S0001           20-MAR-23 P0001                    5       127.2        636 C00001
*/

INSERT INTO Products1 VALUES ('P0002', 'Golden Bowl', 100, NULL, NULL, 50);
INSERT INTO Sales1 VALUES ('S0002', SYSDATE, 'P0002', 60, NULL, NULL, 'C00002');
INSERT INTO Sales1 VALUES ('S0003', SYSDATE, 'P0002', 6, NULL, NULL, 'C00002');
INSERT INTO Sales1 VALUES ('S0004', SYSDATE, 'P0002', 12, NULL, NULL, 'C00002');

/*
PRODUCTCODE     PRODUCTNAME                    PRODUCTPRICE        SST SELLINGPRICE QUANTITYINSTOCK
--------------- ------------------------------ ------------ ---------- ------------ ---------------
P0001           Golden Finger                           120        7.2        127.2              45
P0002           Golden Bowl                             100          6          106              50
*/


CREATE OR REPLACE TRIGGER trg_NewSales
BEFORE INSERT ON Sales1
FOR EACH ROW

DECLARE
v_sellingPrice Products1.SellingPrice%TYPE;
v_qtyInStock   Products1.QuantityInStock%TYPE;
v_custLimit    Customers1.CustCreditLimit%TYPE;
v_custAmount   Customers1.CustCreditAmount%TYPE;

BEGIN
 SELECT sellingPrice, QuantityInStock INTO v_sellingPrice, v_qtyInStock
 FROM Products1
 WHERE ProductCode = :NEW.ProductCode;

 SELECT CustCreditLimit, CustCreditAmount INTO v_custLimit, v_custAmount
 FROM Customers1
 WHERE CustID = :NEW.CustID;

 IF (:NEW.Quantity <= v_qtyInStock) THEN
  :NEW.ActualPrice := v_sellingPrice;
  :NEW.Subtotal := :NEW.ActualPrice * :NEW.Quantity;

  v_custAmount := v_custAmount + :NEW.Subtotal;
  IF (v_custAmount <= v_custLimit) THEN
   UPDATE Products1
   SET QuantityInStock = QuantityInStock - :NEW.Quantity
   WHERE ProductCode = :NEW.ProductCode;

   UPDATE Customers1
   SET CustCreditAmount = CustCreditAmount + :NEW.Subtotal
   WHERE CustID = :NEW.CustID;
  ELSE
   RAISE_APPLICATION_ERROR(-20000, 'Your credit amount : ' || v_custAmount || ' is more than your credit limit: ' || v_custLimit);
  END IF;

 ELSE
  RAISE_APPLICATION_ERROR(-20000, 'Your order quantity is over the quantity in stock: ' || v_qtyInStock);
 END IF;
END;
/


CREATE OR REPLACE TRIGGER trg_track_Products1
AFTER INSERT OR UPDATE ON Products1
DECLARE
timeString VARCHAR(8);

BEGIN
 CASE
  WHEN INSERTING THEN
   timeString := TO_CHAR(SYSDATE, 'HH24:MI:SS');
   INSERT INTO Products1_Audit
   VALUES(USER, SYSDATE, timeString, 'INSERT');
  WHEN UPDATING THEN
   timeString := TO_CHAR(SYSDATE, 'HH24:MI:SS');
   INSERT INTO Products1_Audit
   VALUES(USER, SYSDATE, timeString, 'UPDATE');
  WHEN DELETING THEN
   timeString := TO_CHAR(SYSDATE, 'HH24:MI:SS');
   INSERT INTO Products1_Audit
   VALUES(USER, SYSDATE, timeString, 'DELETE');
  END CASE;
END;
/

INSERT INTO Products1 VALUES('P00003', 'Gold Tea Pot', 899.99, NULL, NULL, 100);
UPDATE Products1 SET ProductPrice = 388.00 WHERE ProductCode = 'P0002';
DELETE FROM Products1 WHERE ProductCode = 'P0003';

/*
PRODUCTCODE     PRODUCTNAME                    PRODUCTPRICE        SST SELLINGPRICE QUANTITYINSTOCK
--------------- ------------------------------ ------------ ---------- ------------ ---------------
P0001           Golden Finger                           120        7.2        127.2              45
P0002           Golden Bowl                             388      23.28       411.28              44

USERID                         TRANSDATE TIME     ACTION
------------------------------ --------- -------- ----------
MONG2N05                       20-MAR-23 13:41:20 INSERT
MONG2N05                       20-MAR-23 13:42:51 UPDATE
MONG2N05                       20-MAR-23 13:45:14 UPDATE
*/


CREATE OR REPLACE TRIGGER trg_track_Customers1
AFTER INSERT OR UPDATE OR DELETE ON Customers1
FOR EACH ROW

BEGIN
 CASE
  WHEN INSERTING THEN
    INSERT INTO InsertCustomers1
    VALUES(:NEW.CustID, :NEW.CustName, :NEW.CustCreditLimit,
 :NEW.CustCreditAmount, USER, SYSDATE , TO_CHAR(SYSDATE, 'HH24:MI:SS'));
 WHEN UPDATING THEN
    INSERT INTO UpdateCustomers1
    VALUES(:OLD.CustID, :OLD.CustName, :NEW.CustName , :OLD.CustCreditLimit, 
:NEW.CustCreditLimit, :OLD.CustCreditAmount,:NEW.CustCreditAmount, USER, SYSDATE , TO_CHAR(SYSDATE, 'HH24:MI:SS'));
 WHEN DELETING THEN
    INSERT INTO DeleteCustomers1
    VALUES(:OLD.CustID, :OLD.CustName, :OLD.CustCreditLimit, 
:OLD.CustCreditAmount, USER, SYSDATE , TO_CHAR(SYSDATE, 'HH24:MI:SS'));
END CASE;
END;
/

INSERT INTO Customers1 VALUES('C00003', 'John Teoh', 1250, 0);
INSERT INTO Customers1 VALUES('C00004', 'Esther Wong', 1000, 0);
UPDATE Customers1 SET CustCreditLimit = 2500 WHERE CustID = 'C00002';
DELETE FROM Customers1 WHERE CustID = 'C00003';

select * from customers1;
select * from insertcustomers1;
select * from updatecustomers1;
select * from deletecustomers1;

/*
CUSTID CUSTNAME                       CUSTCREDITLIMIT CUSTCREDITAMOUNT
------ ------------------------------ --------------- ----------------
C00001 Eric Heng                                 1500              636
C00002 Alice Ooi                                 2500              636
C00004 Esther Wong                               1000                0

CUSTID CUSTNAME                       CUSTCREDITLIMIT CUSTCREDITAMOUNT USERID                         TRANSDATE TRANSTIM
------ ------------------------------ --------------- ---------------- ------------------------------ --------- --------
C00003 John Teoh                                 1250                0 MONG2N05                       20-MAR-23 13:53:12
C00004 Esther Wong                               1000                0 MONG2N05                       20-MAR-23 13:53:18

CUSTID CUSTNAME                       NEWCUSTNAME                    CUSTCREDITLIMIT NEWCUSTCREDITLIMIT CUSTCREDITAMOUNT
------ ------------------------------ ------------------------------ --------------- ------------------ ----------------
NEWCUSTCREDITAMOUNT USERID                         TRANSDATE TRANSTIM
------------------- ------------------------------ --------- --------
C00002 Alice Ooi                      Alice Ooi                                 1000               2500              636
                636 MONG2N05                       20-MAR-23 13:54:35

CUSTID CUSTNAME                       CUSTCREDITLIMIT CUSTCREDITAMOUNT USERID                         TRANSDATE TRANSTIM
------ ------------------------------ --------------- ---------------- ------------------------------ --------- --------
C00003 John Teoh                                 1250                0 MONG2N05                       20-MAR-23 13:54:41
*/