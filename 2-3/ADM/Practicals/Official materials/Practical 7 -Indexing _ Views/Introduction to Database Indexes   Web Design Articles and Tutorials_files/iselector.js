(function($)
{
	function ReplaceSelect(select)
	{
		if($(select).hasClass('ISelectorAlreadyReplaced')) {
			return;
		}

		var replacement = $('<div class="ISelector ISelectorAlreadyReplaced '+select.className+'"></div>');
		var hideSelectReplacement = false;
		if(select.offsetHeight == 0) {
			var clone = select.cloneNode(true);
			$(clone).css({position: 'absolute', left: '-10000px', top: '-10000px', display: 'block'});
			$('body').append(clone);
			offsetHeight = $(clone).height();
			offsetWidth =  $(clone).width();
			$(clone).remove();
			if($(select).css('display') == 'none') {
				hideSelectReplacement = true;
			}
		}
		else {
			offsetHeight = $(select).height();
			offsetWidth = $(select).width();
		}

		if(offsetWidth < 200) {
			offsetWidth = '200px';
		}

		$(replacement).css({height: offsetHeight+'px', width: offsetWidth+'px'});
		if(select.id) {
			$(replacement).attr('id', select.id);
			select.id += '_old';
		}

		if(hideSelectReplacement) {
			$(select).hide();
		}
		replacement.get(0).select = select;
		replacement.get(0).options = select.options;
		replacement.get(0).selectedIndex = select.selectedIndex;
		$(replacement).click(function() {
			$(this.select).trigger('click');
		});
		$(replacement).dblclick(function() {
			$(this.select).trigger('dblclick');
		});
		$(replacement).append('<ul></ul>');
		var list = $(replacement).find('ul');
		$(select).children().each(function(i) {
			if($(this).is('optgroup')) {
				$(list).append(AddOptionGroup(select, this));
			}
			else if($(this).is('option')) {
				$(list).append(AddOption(select, this));
			}
		});
		$(select).hide();
		$(replacement).insertBefore($(select));
	}

	function AddOptionGroup(select, group)
	{
		var extraClass = '';
		if(group.className) {
			extraClass = group.className;
		}
		var html = $('<li class="ISelectorGroup '+extraClass+'"><div>'+$(group).attr('label')+'</div></li>');
		var list = $(html).append('<ul></ul>');
		$(group).find('option').each(function(i) {
			$(list).append(AddOption(select, this));
		});
		return html;
	}

	function AddOption(select, option)
	{
		var value, elementClass, checked = '';
		if($(option).attr('selected')) {
			elementClass = 'SelectedRow';
			checked = 'checked="checked"';
		}
		var label = $(option).html();
		var whitespace = label.match(/^\s*(&nbsp;)*/);
		if(whitespace[0])
		{
			label = label.replace(/^\s*(&nbsp;)*/, '');
		}
		var disabled = '';
		if($(select).attr('disabled')) {
			var disabled = ' disabled="disabled"';
		}
		var extraKey = '';
		var extra = '';
		if(option.className & option.className.indexOf('forceKey') != -1) {
			var start = option.className.indexOf('forceKey');
			var end = option.className.indexOf(' ', start+1);
			if(end == -1) {
				var end = option.className.length;
			}
			var extraKey = option.className.substring(start+8, end);
			var extra = '[]';
		}
		var iOption = $('<li id="ISelector'+$(select).attr('name').replace('[]', '')+'_'+$(option).val()+'">');
		$(iOption).addClass(elementClass);
		$(iOption).mouseover(function() {
			OptionHover(this, 'over');
		});
		$(iOption).mouseout(function() {
			OptionHover(this, 'out');
		})
		$(iOption).bind('selectstart', function() {
			return false;
		});
		$(iOption).click(function(e) {
			if(this.dblClickTimeout) {
				return false;
			}
			if(e.target.tagName != 'INPUT') {
				var checkbox = $(this).find('input');
				checkbox.attr('checked', !checkbox.attr('checked'));
			}
			OptionClick(this);
		});
		$(iOption).dblclick(function(e) {
			var option = $(this).data('option');
			$(option).trigger('dblclick');
		});
		$(iOption).append(whitespace[0]);
		$(iOption).data('option', option);
		if($(option).hasClass('freeform')) {
			$(iOption).append('<input type="textbox" name="ISelector_'+select.name+'['+extraKey+']'+extra+'" value="'+$(option).val()+'" />');
		}
		else {
			if($(select).attr('multiple')) {
				$(iOption).append('<input type="checkbox" name="ISelector_'+select.name+'['+extraKey+']'+extra+'" value="'+$(option).val()+'" '+checked+disabled+' />'+label);
			}
			else {
				$(iOption).append('<input type="radio" name="ISelector_'+select.name+'['+extraKey+']'+extra+'" value="'+$(option).val()+'" '+checked+disabled+' />'+label);
			}
		}
		return iOption;
	}

	function OptionHover(element, action)
	{
		if(action == 'out') {
			$($(element).data('option')).trigger('mouseout');
			$(element).removeClass('ISelectorOptionHover');
		}
		else {
			$($(element).data('option')).trigger('mouseover');
			if(!$(element).hasClass('SelectedRow')) {
				$(element).addClass('ISelectorOptionHover');
			}
		}
	}

	function ScrollToItem(select, value, group)
	{
		var item = $('#ISelector'+select.replace('[]', '')+'_'+value);
		if(!item.get(0)) {
			return;
		}
		var replacement = item.parents('div.ISelector');
		var position = item.position().top;
		if(group != undefined && group == true) {
			position -= 20;
		}
		replacement.scrollTop(position);
	}

	function OptionClick(element) {
		var replacement = $(element).parents('.ISelector').get(0);
		var option = $(element).data('option');
		var checkbox = $(element).find('input');
		element.dblClickTimeout = setTimeout(function() { element.dblClickTimeout = ''; }, 250);
		$(option).attr('selected', $(checkbox).attr('checked'));
		replacement.selectedIndex = replacement.select.selectedIndex;
		if(checkbox.attr('checked')) {
			if(!$(replacement.select).attr('multiple')) {
				$(replacement).find('li.SelectedRow').removeClass('SelectedRow');
			}
			$(element).addClass('SelectedRow');
		}
		else {
			$(element).removeClass('SelectedRow');
		}
		$(option).trigger('click');
		$(option).parents('select').trigger('change');
	}

	$.fn.iselector = function(settings)
	{
		return this.each(function() {
			ReplaceSelect(this);
		});
	}

	$.fn.iselectorAddItem = function(options)
	{
		return this.each(function() {
			var id = $(this).attr('id');
			var select = $('#'+id+'_old').get(0);
			var newOption = $('<option value="'+options.value+'">'+options.label+'</option>');
			var option = newOption.get(0);
			$(select).append(newOption);
			var newIOption = AddOption(select, option);
			$(this).children('ul').append(newIOption);
			if(options.selected != undefined && options.selected == true) {
				$(newOption).attr('selected', true);
				newIOption.find('input').attr('checked', true);
				OptionClick(newIOption);
				$(select).trigger('change');
			}
			if(options.scrollTo != undefined && options.scrollTo == true) {
				ScrollToItem(select.name, options.value);
			}
		});
	}

	$.fn.iselectorRemoveItem = function(value)
	{
		var values = jQuery.makeArray(value);
		return this.each(function() {
			var id = $(this).attr('id');
			var select = $('#'+id+'_old').get(0);
			var fireChange = false;
			$(values).each(function() {
				var option = $(select).find('option[value="'+this+'"]');
				if($(option).attr('checked')) {
					fireChange = true;
				}
				option.remove();
				var item = $('#ISelector'+select.name.replace('[]', '')+'_'+this);
				$(item).remove();

			});
			$(this).get(0).selectedIndex = select.selectedIndex;
			if(fireChange) {
				$(select).trigger('change');
			}
		});
	}

	$.fn.iselectorRemoveGroup = function(value)
	{
		var values = jQuery.makeArray(value);
		return this.each(function() {
			var id = $(this).attr('id');
			var select = $('#'+id+'_old').get(0);
			$(values).each(function() {
				$(select).find('option[value="'+this+'"]').parents('optgroup').remove();
				$('#ISelector'+select.name.replace('[]', '')+'_'+this).parents('li.ISelectorGroup').remove();
			});
			$(this).get(0).selectedIndex = select.selectedIndex;
			$(select).trigger('change');
		});
	}

	$.fn.iselectorScrollTo = function(value)
	{
		return this.each(function() {
			var id = $(this).attr('id');
			var select = $('#'+id+'_old').get(0);
			ScrollToItem(select.name, value);
		});
	}

	// Replace the existing jQuery() function with our own wrapped version
	$.fn.oldVal = jQuery.fn.val;
	$.fn.val = function(value)
	{
		if(value == undefined) {
			if($(this).hasClass('ISelector')) {
				var id = '#'+$(this).attr('id')+'_old';
				return $(id).oldVal();

			}
			return $(this).oldVal();
		}
		else {
			if($(this).hasClass('ISelector')) {
				var id = '#'+$(this).attr('id')+'_old';
				var ret = $(id).oldVal(value);
				// OK, now we've performed the select on the old version,
				// we need to match up the iSelector equivilents and select them too
				var values = jQuery.makeArray(value);
				$(this).find('li > input').each(function() {
					if($.inArray(this.value, values) >= 0) {
						this.checked = true;
					}
					else {
						this.checked = false;
					}
					OptionClick($(this).parent('li'));
				});
			}
			return $(this).oldVal(value);
		}
	}
})(jQuery);

