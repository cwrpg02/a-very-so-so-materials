db.book.find()

db.book.find().pretty()

db.book.find({title: "Goodbye Things"})

db.book.find({title: "Goodbye Things"}).pretty()


db.book.find({$and:[{publisher: "Penguin"},
		  {author: "Fumio Sasaki"}]})

db.book.find({$or:[{publisher: "Penguin"},
		{publisher: "Avery"}]})

db.book.find({book_id: {$gte: 2000}})

db.book.find({price: {$gte: 60}})

db.book.find({publisher: {$in: ["Avery", "Penguin"]}})

db.book.count({book_id:{$gte: 1000}})

db.book.count({price:{$gte: 60}})

db.book.aggregate([{$group: {_id: null,
		highestPrice: {$max: "$price"}} 
		}])

db.book.aggregate([{$group: {_id: "$publisher",
		PriceOfMostExpensiveBook: {$max: "$price"}} 
		}])

db.book.aggregate([{$group: {_id: null,
		CheapestBook: {$min: "$price"},
		MostExpensiveBook:{$max: "$price"}} 
		}])

db.book.aggregate([{$group: {_id: null,		
		TotalBooksInBookstore:{$sum: 1}} 
		}])

db.book.aggregate([{$project:{ _id: 0,
			title: 1,
			author: 1}}])

db.book.aggregate([{$project: { _id: 0,
		    title: 1,		    
		    NumberOfAuthors:{$size: "$author"}}
		}])




