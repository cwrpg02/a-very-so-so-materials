--Using cursor to print sets of records
--In this example, we want to print product details and indicate which product has UnitsInStock lower than a certain amount
/*
CREATE TABLE products(
productCode		varchar(15) NOT NULL,
productName		varchar(70) NOT NULL,
productLine		varchar(50) NOT NULL,
productScale		varchar(10) NOT NULL,
productVendor		varchar(50) NOT NULL,
productDescription	varchar(4000) NOT NULL,
quantityInStock		number(4) NOT NULL,
buyPrice		number(7,2) NOT NULL,
MSRP			number(7,2) NOT NULL,
PRIMARY KEY (productCode)
);
*/

CREATE OR REPLACE PROCEDURE prc_Low_Stock(v_lowQty IN NUMBER) IS

v_prodCode	PRODUCTS.productCode%TYPE;
v_prodName	PRODUCTS.productName%TYPE;
v_prodLine	PRODUCTS.ProductLine%TYPE;
v_prodScale	PRODUCTS.productScale%TYPE;
v_prodVendor	PRODUCTS.productVendor%TYPE;
v_prodqty	PRODUCTS.quantityInStock%TYPE;
v_buyPrice	PRODUCTS.buyPrice%TYPE;

v_indicator     char(5);

CURSOR PROD_CURSOR IS
   SELECT productCode, productName, productLine, productScale, productVendor, quantityInStock, buyPrice
   FROM PRODUCTS;

BEGIN
DBMS_OUTPUT.PUT_LINE('PRODUCTS RUNNING LOW ON STOCK');
DBMS_OUTPUT.PUT_LINE('=============================');
DBMS_OUTPUT.PUT_LINE('====PLS=====INSERT==SUB-HEADING===HERE===============');

OPEN PROD_CURSOR;

LOOP
   FETCH PROD_CURSOR INTO v_prodCode,v_prodName,v_prodLine,v_prodScale,v_prodVendor,v_prodQty,v_buyPrice;
   EXIT WHEN PROD_CURSOR%NOTFOUND;
   if v_prodQty < v_lowQty then
      v_indicator := '#<---';
   else
      v_indicator := '     ';
   end if;
   DBMS_OUTPUT.PUT_LINE(v_prodCode||'-'||v_prodName||'-'||v_prodLine||'-'||v_prodScale||'-'||v_prodVendor||'-'||v_prodQty||'-'||v_buyPrice||v_indicator);
END LOOP;

DBMS_OUTPUT.PUT_LINE('======================================');
DBMS_OUTPUT.PUT_LINE('TOTAL PRODUCTS PROCESSED ' || PROD_CURSOR%ROWCOUNT); 
DBMS_OUTPUT.PUT_LINE('# indicates products low in quantity -- less than :'||v_lowQty); 
DBMS_OUTPUT.PUT_LINE('--- END OF REPORT ----');
CLOSE PROD_CURSOR;
END;
/
