You must have Oracle SQL installed.

1. To download Oracle, click here: https://drive.google.com/file/d/0B7EvzsW9KgAzbFlKQ1htOU8zaFk/view?resourcekey=0-jjGJLnoxQ3SvX0qc1GgcLQ
2. Follow the instructions in Installation Guide file.
3. We'll mainly use SQL Plus for this course, which usually can be found at this path: C:\oraclexe\app\oracle\product\11.2.0\server\bin\sqlplus.exe
4. Make sure to create shortcut for sqlplus.exe and put it anywhere that can be easily found.