Other materials needed for practical use:
1. FindBugs 3.0.1 - http://prdownloads.sourceforge.net/findbugs/findbugs-3.0.1.zip?download
2. Apache JMeter 5.2.1 - https://archive.apache.org/dist/jmeter/source/apache-jmeter-5.2.1_src.zip
3. ChromeDriver 96.0.4664.45 - https://chromedriver.storage.googleapis.com/index.html?path=96.0.4664.45/
4. Selenium Java 4.0.0 - https://github.com/SeleniumHQ/selenium/releases/download/selenium-4.0.0/selenium-java-4.0.0.zip