
import java.util.HashMap;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author TARUC
 */
public class Configuration {
    
    private HashMap<String, String> keyValuePairs;
    private int flag = 0;
    private double top = 0.00;
   public Configuration()   
   {
     keyValuePairs = new HashMap<String, String>();
     // load config from file
   }
   
   public HashMap<String, String> getValues()
   {
     return keyValuePairs;
   }
   
   public String getValue(String key)
   {
     if (keyValuePairs.containsKey(key))
       return keyValuePairs.get(key);
     else 
       return null;
   }
   
   public void setValue(String key, String value)
   {
     if (keyValuePairs.containsKey(key))
       keyValuePairs.replace(key, value);
     else
       keyValuePairs.put(key, value);
     writerConfig();
   }

    private void writerConfig() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
