﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Prac1
{
    public partial class P3_BusTicketing : System.Web.UI.Page
    {
        // display current date and time - if false, displays everytime click "Postback"; no display otherwise
        Boolean IsPostBack = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            // first time page is loaded = IsPostBack (false)
            if (IsPostBack == false) // can also type as - if (!IsPostBack)
            {
                lblTime.Text = "You are accessing this page on " + DateTime.Now.ToString();

                // to display today's date by default when the page is loaded for the first time
                txtDepartDt.Text = DateTime.Now.ToShortDateString();
            }
        }

        protected void calDepartDt_SelectionChanged(object sender, EventArgs e)
        {
            txtDepartDt.Text = calDepartDt.SelectedDate.ToShortDateString();
        }

        protected void ddlTo_SelectedIndexChanged(object sender, EventArgs e)
        {
            // calculate the total price
            // HD to Butter: 34.00 for adult, 25.50 for child
            // HP to Seremban: 6.00 for adult, 4.30 for child

            if (ddlFrom.Text == "Hentian Duta" && ddlTo.Text == "Butterworth")
            {
                txtPrice.Text = "RM " + String.Format("{0:0.00}", ((Double.Parse(txtAdult.Text) * 34.00) + (Double.Parse(txtChild.Text) * 25.50)));
                lblError.Text = "";
            }
            else if (ddlFrom.Text == "Hentian Duta" && ddlTo.Text == "Seremban")
            {
                txtPrice.Text = "";
                lblError.Text = "Sorry. We do not provide trip from Hentian Duta to Seremban";
            }
            else if (ddlFrom.Text == "Hentian Putra" && ddlTo.Text == "Butterworth")
            {
                txtPrice.Text = "";
                lblError.Text = "Sorry. We do not provide trip from Hentian Putra to Butterworth";
            }
            else if (ddlFrom.Text == "Hentian Putra" && ddlTo.Text == "Seremban")
            {
                txtPrice.Text = "RM " + String.Format("{0:0.00}", ((Double.Parse(txtAdult.Text) * 6.00) + (Double.Parse(txtChild.Text) * 4.30)));
                lblError.Text = "";
            }
            else
            {
                txtPrice.Text = "";
                lblError.Text = ddlFrom.Text + " to " + ddlTo.Text + " is not a valid journey";
            }
        }
    }
}