﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrol.aspx.cs" Inherits="Prac2.servercontrol" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 220px;
        }
        .auto-style2 {
            width: 220px;
            height: 81px;
        }
        .auto-style3 {
            height: 81px;
        }
        .auto-style4 {
            width: 220px;
            height: 30px;
        }
        .auto-style5 {
            height: 30px;
        }
        .auto-style6 {
            width: 220px;
            height: 23px;
        }
        .auto-style7 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width:100%;">
                <tr>
                    <td class="auto-style1">Name:</td>
                    <td>
                        <asp:TextBox ID="txtName" runat="server" Width="188px"></asp:TextBox>
                    &nbsp;// textbox ID = txtName</td>
                </tr>
                <tr>
                    <td class="auto-style1">Program:</td>
                    <td>
                        <asp:DropDownList ID="ddlProg" runat="server">
                            <asp:ListItem>RIS</asp:ListItem>
                            <asp:ListItem>RSD</asp:ListItem>
                            <asp:ListItem>RIT</asp:ListItem>
                            <asp:ListItem>RSF</asp:ListItem>
                            <asp:ListItem>REI</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        // dropdownlist = ddlProg</td>
                </tr>
                <tr>
                    <td class="auto-style2">Area of Interest:</td>
                    <td class="auto-style3">
                        <asp:CheckBoxList ID="cblInterest" runat="server">
                            <asp:ListItem>Programming</asp:ListItem>
                            <asp:ListItem>Networking</asp:ListItem>
                            <asp:ListItem>Database</asp:ListItem>
                        </asp:CheckBoxList>
                        <br />
                        // checkboxlist ID = cblInterest</td>
                </tr>
                <tr>
                    <td class="auto-style4"></td>
                    <td class="auto-style5">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                    &nbsp;// button ID = btnSubmit; text = Submit</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style6"></td>
                    <td class="auto-style7">
                        <asp:Label ID="lblSelect" runat="server" Text="[lblSelect]"></asp:Label>
                    &nbsp;// Label ID = lblSelect</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
