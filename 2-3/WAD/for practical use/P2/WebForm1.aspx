﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Prac2.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style8 {
        height: 33px;
        width: 111px;
    }
    .auto-style9 {
        height: 24px;
        width: 111px;
    }
    .auto-style10 {
        height: 84px;
        width: 111px;
    }
    .auto-style11 {
        height: 3px;
        width: 111px;
    }
    .auto-style12 {
        height: 34px;
        width: 111px;
    }
    .auto-style13 {
        height: 33px;
        width: 877px;
    }
    .auto-style14 {
        height: 24px;
        width: 877px;
    }
    .auto-style15 {
        height: 84px;
        width: 877px;
    }
    .auto-style16 {
        height: 3px;
        width: 877px;
    }
    .auto-style17 {
        height: 34px;
        width: 877px;
    }
    .auto-style18 {
        width: 877px;
    }
    .auto-style19 {
        width: 111px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
    <table style="width:100%;">
                <tr>
                    <td class="auto-style8">Name:</td>
                    <td class="auto-style13">
                        <asp:TextBox ID="txtName" runat="server" Width="188px"></asp:TextBox>
                    &nbsp;// textbox ID = txtName</td>
                </tr>
                <tr>
                    <td class="auto-style9">Program:</td>
                    <td class="auto-style14">
                        <asp:DropDownList ID="ddlProg" runat="server">
                            <asp:ListItem>RIS</asp:ListItem>
                            <asp:ListItem>RSD</asp:ListItem>
                            <asp:ListItem>RIT</asp:ListItem>
                            <asp:ListItem>RSF</asp:ListItem>
                            <asp:ListItem>REI</asp:ListItem>
                        </asp:DropDownList>
                        <br />
                        // dropdownlist = ddlProg</td>
                </tr>
                <tr>
                    <td class="auto-style10">Area of Interest:</td>
                    <td class="auto-style15">
                        <asp:CheckBoxList ID="cblInterest" runat="server">
                            <asp:ListItem>Programming</asp:ListItem>
                            <asp:ListItem>Networking</asp:ListItem>
                            <asp:ListItem>Database</asp:ListItem>
                        </asp:CheckBoxList>
                        <br />
                        // checkboxlist ID = cblInterest</td>
                </tr>
                <tr>
                    <td class="auto-style11"></td>
                    <td class="auto-style16">
                        <asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit" />
                    &nbsp;// button ID = btnSubmit; text = Submit</td>
                </tr>
                <tr>
                    <td class="auto-style12"></td>
                    <td class="auto-style17"></td>
                </tr>
                <tr>
                    <td class="auto-style19"></td>
                    <td class="auto-style18">
                        <asp:Label ID="lblSelect" runat="server" Text="[lblSelect]"></asp:Label>
                    &nbsp;// Label ID = lblSelect</td>
                </tr>
            </table>
    </div>
</asp:Content>
