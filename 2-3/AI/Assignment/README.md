This folder is for AI assignment and is a work in progress.

# Info
> Group title
> 
> Title: Sunway Pyramid Customer Service Chatbot<br>
> Topic: Chatbot Development

> Individual titles
> 
> My Module: Probability theory algorithm<br>
> Member 1 Module: ANN algorithm<br>
> Member 2 Module: Depth-first search

For source codes, see prototype folder. Go to `me` subfolder for my work; the UI (html and css files) is designed by me too.