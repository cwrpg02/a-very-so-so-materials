import re
import random
import pandas as pd
import math
import os
from flask import Flask, render_template, request

dataSet = pd.read_csv("dataSet.csv", encoding="unicode_escape")

def msg_probability(msginput, keyword, required=[]):
    msg_certainty = 0
    has_required_words = True

    # Count how many words are present in message
    for word in msginput:
        if word in keyword:
            msg_certainty += 1

    # Calculate the percentage of keywords in the input
    percentage = float(msg_certainty) / float(len(keyword))

    # Check whether the required words are in the string
    if (len(required) < 1):
        for word in required:
            if word not in msginput:
                has_required_words = False
                break

    # Must either have required words
    if has_required_words:
        return int(percentage * 100)
    else:
        return 0


def checkmsg(msg):
    highest_prob = {}

    # Simplifies response creation / adds it to the dictionary
    def response(bot_response, words, required=[]):
        nonlocal highest_prob
        highest_prob[bot_response] = msg_probability(msg, words, required)

    # Check whether the required word is empty
    for i in range(len(dataSet)):
        required_word = ""
        if type(dataSet["Required"][i]) is str:
            required_word = dataSet["Required"][i].split(",")

        else:
            required_word = [""]

        response(dataSet["Response"][i], str(dataSet["Keyword"][i]).split(","), required_word)

    best = max(highest_prob, key=highest_prob.get)
    return randommsg() if highest_prob[best] < 1 else best


# Get the response
def get_response(msginput):
    # Remove all symbols from the input message
    split = re.split(r'\s+|[,;?!.-]\s*', msginput.lower())
    bot_response = checkmsg(split)
    return bot_response


# Return a random message when input is not in dataset
def randommsg():
    response = ["Sorry, I cannot understand that.",
                "What does that mean?",
                "Could you re-phrase that?",
                "...",
                "???"]
    return response[random.randrange(5)]


# Test the response system
app = Flask(__name__)
app.static_folder = 'static'


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/get")
def get_bot_response():
    msginput = request.args.get('botMsg')
    return str(get_response(msginput))


if __name__ == "__main__":
    os.system("start http://127.0.0.1:5000/")
    app.run()

while True:
    response = get_response(input('You: '))
    print('Chatbot: ', response)
