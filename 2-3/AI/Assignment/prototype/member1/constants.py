# File paths
EMOTION_DIR_PATH = "res/emotions"
BEST_MODEL_DIR_PATH = "build/best_model"
LAST_MODEL_DIR_PATH = "build/last_model"
BEST_VAL_ACC_FILE_PATH = "build/best_val_acc.txt"
BACKUP_DIR_PATH = "tmp/backup"
REPORT_DIR_PATH = "reports"
TRAIN_LOG_FILE_PATH = "build/train_log.csv"

# Emotion types
LABELS = [
    "Neutral",
    "Happiness",
    "Surprise",
    "Sadness",
    "Anger",
    "Disgust",
    "Fear",
    "Contempt",
    "Unknown",
    "NF"
]
