from keras import callbacks
from itertools import islice
import random
import json
import numpy as np
import pickle
import csv
import matplotlib.pyplot as plt
import nltk
import constants
nltk.download('punkt')
nltk.download('wordnet')
nltk.download('omw-1.4')
import tensorflow as tf
from keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score
from nltk.stem import WordNetLemmatizer

from keras.models import Sequential
from keras.layers import Dense, Activation, Dropout
from keras.optimizers import Adam
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, BackupAndRestore, CSVLogger, EarlyStopping

# Example of k-fold cross-validation in Keras
from sklearn.model_selection import StratifiedKFold

lemmatizer = WordNetLemmatizer()
intents = json.loads(open("intents.json").read())

words = []  # store all the tokenize pattern
classes = []  # store all the label in the json file
documents = []  # store the pair of tokenize pattern list and the tag
ignore_letters = ['?', '!', '.', ',']

# Read the Json file and store inside the variable
for intent in intents["intents"]:
    for pattern in intent["patterns"]:
        word_list = nltk.word_tokenize(pattern)  # tokenize the word from a sentense
        words.extend(word_list)
        documents.append((word_list, intent["tag"]))
        if intent["tag"] not in classes:
            classes.append(intent["tag"])

words = [lemmatizer.lemmatize(word.lower()) for word in words if word not in ignore_letters]
words = sorted(list(set(words)))
classes = sorted(list(set(classes)))

pickle.dump(words, open("words.pkl", "wb"))
pickle.dump(classes, open("classes.pkl", "wb"))

trainning = []
output_empty = [0] * len(classes)

# find the pattern of the word in order use for later ANN
for doc in documents:
    bag = []
    word_pattern = doc[0]
    word_pattern = [lemmatizer.lemmatize(word.lower()) for word in word_pattern]
    for word in words:
        bag.append(1) if word in word_pattern else bag.append(0)

    output_row = list(output_empty)
    output_row[classes.index(doc[1])] = 1
    trainning.append([bag, output_row])

random.shuffle(trainning)
trainning = np.array(trainning, dtype=object)

# train_x = list(trainning[:, 0])
# train_y = list(trainning[:, 1])
train_x = list(trainning[:, 0])
train_y = list(trainning[:, 1])
train_x = np.array(train_x)
train_y = np.array(train_y)

# Define the ANN model
model = Sequential()

model.add(Dense(299, input_shape=(len(train_x[0]),), activation='relu'))
model.add(Dropout(0.1))
# model.add(Dense(64, activation='relu'))
model.add(Dense(64, activation='relu'))
model.add(Dropout(0.1))
model.add(Dense(749, activation="softmax"))
# model.add(Dense(len(train_y[0]), activation="sigmoid"))

sgd = SGD(learning_rate=0.021, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(loss="categorical_crossentropy", optimizer=sgd, metrics=['accuracy'])
# model.compile(loss="categorical_crossentropy", optimizer=Adam(learning_rate=0.001), metrics=['accuracy'])

earlystopping = callbacks.EarlyStopping(monitor ="val_loss", 
                                         mode ="min", 
                                         restore_best_weights = True)

# Train the ANN
history=model.fit(
    x=np.array(train_x),
    y=np.array(train_y),
    batch_size=2,
    epochs=100,
    # validation_data = (val_x, val_y),
    validation_split=0.25,
    
    callbacks=[
        ModelCheckpoint(
            filepath=constants.BEST_MODEL_DIR_PATH,
            monitor="val_accuracy",
            verbose=1,
            save_best_only=True,
        ),
        BackupAndRestore(backup_dir=constants.BACKUP_DIR_PATH),
        ReduceLROnPlateau(factor=1.0 / 3.0, verbose=1),
        CSVLogger(filename=constants.TRAIN_LOG_FILE_PATH, append=True)
    ]
)

with open(constants.TRAIN_LOG_FILE_PATH) as train_log_file:
    epoches = []
    accs = []
    val_accs = []
    losses = []
    val_losses = []

    for row in islice(csv.reader(train_log_file), 1, None):
        [epoch, acc, loss, _, val_acc, val_loss] = row
        epoches.append(int(epoch) + 1)
        accs.append(float(acc))
        val_accs.append(float(val_acc))
        losses.append(float(loss))
        val_losses.append(float(val_loss))

    # Summarize history for loss and save the result
    print("Generating model loss report...")
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('Model Loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(f"{constants.REPORT_DIR_PATH}/model-loss.png")
    plt.close()

    # Summarize history for accuracy and save the result
    print("Generating model accuracy report...")
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('Model Accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.savefig(f"{constants.REPORT_DIR_PATH}/model-acc.png")
    plt.close()


model.save("chatbotModel.h5",history)
print("Done")

