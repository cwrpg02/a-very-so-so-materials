import random
import json
import numpy as np
import pickle
from flask import Flask, render_template, request
import os

import nltk
from nltk.stem import WordNetLemmatizer

from keras.models import load_model

lemmatizer = WordNetLemmatizer()
intents = json.loads(open("intents.json").read())

words=pickle.load(open("words.pkl","rb"))
classes=pickle.load(open("classes.pkl","rb"))
model=load_model("chatbotModel.h5")

def clean_up_sentense(sentence):
    sentence_word=nltk.word_tokenize(sentence)
    sentence_word=[lemmatizer.lemmatize(word.lower()) for word in sentence_word]
    return  sentence_word

def bag_of_word(sentence):
    sentence_word=clean_up_sentense(sentence)
    bag=[0]*len(words)
    for s in sentence_word:
        for i, word in enumerate(words):
            if word==s:
                bag[i]=1
    return np.array(bag)


def prefict_class(sentence):
    bow=bag_of_word(sentence)
    res=model.predict(np.array([bow]))[0]
    ERROR_THRESHOLD=0.1
    result=[[i,r] for i,r in enumerate(res) if r> ERROR_THRESHOLD]
    result.sort(key=lambda x:x[1],reverse=True)

    return_list=[]
    for r in result:
        return_list.append({"intent":classes[r[0]],"probability":str(r[1])})

    return  return_list

def get_response( intents_list, intents_json):
    try:
        tag = intents_list[0]['intent']
        list_of_intents = intents_json['intents']
        for i in list_of_intents:
            if i['tag'] == tag:
                result = random.choice(i['responses'])
                break
    except IndexError:
        result = "I don't understand! Please Key in again\n If you facing any problem, feels free to contact us <a href='tel:+60374943100'>+03 7494 3100</a>"
    return result

msg=""
   
app = Flask(__name__)
app.static_folder = 'static'

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/get")
def get_bot_response():
    userText = request.args.get('msg')
    ints=prefict_class(userText)
    return str(get_response(ints,intents))

if __name__ == "__main__":
    os.system("start http://127.0.0.1:5000/")
    app.run()
    
     

